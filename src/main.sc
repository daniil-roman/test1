require: patterns.sc
    module = common

require: patterns/outboundPatterns.sc
    module = outbound-common

require: patterns/statePatterns.sc
    module = outbound-common

require: scripts/function.js
    module = outbound-common

require: scripts/answer.js
    module = outbound-common

require: obsceneCheck.sc
    module = outbound-common

require: checkForRepeatsInAnswers.sc
    module = outbound-common

require: instantAgreeLogs.sc
    module = outbound-common

require: report.sc
    module = outbound-common
    injector = {crm_template: {acceptedCode: "50", rejectedCode: "51"}}

require: scripts/moment.min.js
require: scripts/function.js
require: scripts/status.js

require: where/where.sc
    module = common

require: city/city.sc
    module = common

require: dictionaries/regions.csv
    name = $Regions

require: patterns/localPatterns.sc

require: almostAccepted.sc
    module = outbound-common
    injector = {voicе: "olga", stateToGo: "/Objections/Suggest"}

require: welcomePhrase.sc
    module = outbound-common
    injector = {voice: "olga pku"}

require: bargeIn.sc
    module = outbound-common
    injector = {voice: "olga", bargeInIf: true, stateToGo: "/Objections/Suggest"}

require: callOnHold.sc
    module = outbound-common

require: redial/redial.sc
    module = outbound-common

require: number/number.sc
    module = common

require: dateTime/moment.min.js
    module = common

require: namesRu/namesRu.sc
    module = common

init:
    $global.$ = {
        __noSuchProperty__: function(property) {
            return $jsapi.context()[property];
        }
    };

    $global.audioTranslator.__noSuchProperty__ = function(property) {
            log("WARN! No endStatus for reply: " + property);
            return "Не определено: " + property;
       };

    $global.CATCHALL_LIMIT = $injector.catchAllLimit;
    $global.OBJECTION_LIMIT = $injector.objectionLimit;

    bind("preMatch", function($context) {
        if ($context.session.speechNotRecognizedInRow === 1
            && $context.request.event === "speechNotRecognized") {
            $context.temp.targetState = "/Start/ProbablyAnsweringMachine/ItsAnsweringMachine";
        }
    });

    bind("preProcess", function($context) {
        if ($context.currentState.endsWith("ProbablyAnsweringMachine")) {
            $context.session.speechNotRecognizedInRow = $context.session.speechNotRecognizedInRow || 0;
            $context.session.speechNotRecognizedInRow += 1;
        } else {
            delete $context.session.speechNotRecognizedInRow;
        }
    });

    bind("postProcess", function($context) {
        $context.session.state2return = $context.contextPath;
        $context.session.prevAnswer = $context.response.answer;
        $context.session.lastState = $context.currentState;

        var callResult = $context.temp.definedEndStatus || $context.temp.endStatusByAnswer;

        if (callResult) {
            $dialer.setCallResult(callResult);
        }
    });

    bind("onAnyError", function($context) {
        if (!testMode()) {
            log("ERROR! " + $context.exception.message);
            $reactions.audio("https://248305.selcdn.ru/demo_bot_static/PKU_30.03_В итоге ничего не расслышали_01.wav");
            $context.temp.definedEndStatus = "CALLBACK by Error";
            if ($context.exception.message.indexOf("redial") === -1) {
                setRedialTime();
            }
            hangUp();
        } else {
            throw "ERROR! " + $context.exception.message;
        }
    });

theme: /

    state: Events_CLIENT_HANGUP
        event!: hangup
        event: hangup || fromState = /Start, onlyThisState = true
        script:
            log($client.callAttempt);

    state: Abroad
        q: $abroad
        q: ((в/на) $Country/за границей/заграница)
        if: ($parseTree.value === 'notInCountry' && $parseTree.Country[0].value.name === 'Россия') || ($parseTree.value !== 'notInCountry' && ($parseTree.Country ? $parseTree.Country[0].value.name !== 'Россия' : true))

            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                log("Client is abroad");
                $temp.definedEndStatus = "За границей";
                hangUp();

        else:
            go!: /Start/Yes

    state: Start
        #q!: * *start
        script:
            $jsapi.startSession();
            _.extend($session, {
                timesOfNo: 0, // Счётчик общих отказов
                timesOfLongNo: 0 // Счётчик возражений
            });
            $temp.definedEndStatus = "start";

        # 1814400000 = 21 days
        if: !$client.fisrtCallTime || (getNowTimestamp() - $client.fisrtCallTime > 1814400000)
           script:
               delete $client.callAttempt;

        script:
            addPause();
            $temp.definedEndStatus = "Приветствие + уточняем удобно ли говорить";

        if: $client.callAttempt
            if: $client.callAttempt === 2
                audio: PKU_30.03_Алло, здравствуйте перезвон-1_01.wav
                script:
                    $client.callAttempt += 1;
            elseif: $client.callAttempt === 3
                audio: PKU_30.03_Алло, здравствуйте перезвон-2_01.wav
                script:
                    $client.callAttempt += 1;
            else:
                audio: PKU_30.03_Алло, здравствуйте перезвон-3_01.wav
                script:
                    delete $client.callAttempt;
        else:
            audio: PKU_30.03_Алло, здравствуйте_01.wav
            script:
                $client.fisrtCallTime = getNowTimestamp();
                $client.callAttempt = 2;

        state: NoInterest
            q: $noCallInterest
            q: * {(не (*надо*/нужн*)) * (ничего/никто) (претензи*) * (нет/нету/отсутствую*)} * $weight<+0.4>
            q: * {[$disagree] (не (*надо*/нужн*)) * спасибо} * $weight<+0.1>
            q: * {(~старый/~древний/кнопочн*/~старенький) $phone} * $weight<+0.1>
            q: * (не смартфон*) *
            q: * {(простой/простенький/кнопочн*/кнопка*/~старенький) * $phone} * $weight<+0.1>
            q: * пожалуй нет *
            q: * {зачем * (надоеда*/~приставать)} *
            q: * ($yes/говори*/уделю/уделя*/слушаю/как*/что/чего/какое/постара*/может быть/можно) * (хотя/вообще/а нет/передумал*/впрочем/но) * (не удел*/$noCallInterest/$stateDisagreeWeak) *
            q: * {$noCallInterest * минут* (будет/найдется/можно)} *
            q: * {(давай* не [буде*]/хватит/прекрати*) * трат* * ~время} *
            q: * я [тогда/в таком случае/буду/пожалуй] $Name $mobileOperator *
            q: * если * (прийти/идти/приходить) * (не *могу) *
            audio: 22_05_неинтересно_перед_предложением_Ольга.wav
            script:
                $session.noInterest = true;
                $temp.definedEndStatus = "Абоненту не интересно в 1-ый раз (до предложения)";

            state: YesAfterNoInterest
                q: $agreeIfNoInterestFirst $weight<+0.1> || fromState = "..", onlyThisState = true
                go!: /Start/Yes

            state: No_AfterNoInterest || modal = true
                q: (* $disagree */$noCallInterest) [$bye] $weight<+0.5>
                q: * $weight<+0.1>
                audio: 22_05_Отвечает_нет_после_мне_не_интересно_до_предложения-Ольга.wav
                script:
                    $temp.definedEndStatus = "Абоненту не интересно во 2-ой раз (до предложения)";

                state: Callback
                    q: $agreeIfNoInterestSecond
                    q: * (когда/на сколько/насколько) *позже *
                    q: * (когда/другой раз/через $Number) [$AnyWord] [$AnyWord] [*звони*]
                    go!: /Objections/CallBackLater

                state: NoLastTime
                    q: *
                    q: (* $disagree */$noCallInterest)
                    q: * (не [очень] удобно/неудобно) *
                    q: $noMoreCalls
                    audio: PKU_30.03_Абонент отказался_01.wav
                    script:
                        $temp.definedEndStatus = "Абоненту не интересно в 3-й раз (до предложения)";
                        hangUp();

            state: LocalCatchAll
                event: speechNotRecognized || fromState = /Start/NoInterest
                q: * [$disagree] * $noHaveNoTime $weight<+0.2>  || fromState = /Start/NoInterest
                q: * (повтори/повторите/не расслыш*) * || fromState = /Start/NoInterest
                q: * (повтори/повторите/не расслыш*) * || fromState = /Start/NoInterest/No_AfterNoInterest
                go!: /Objections/CallBackLater

        state: BargeIn || modal = true
            event: bargeInIntent
            script:
                log("!!!!! bargeIn before proposal ");
            # Если приветствие перебил автоответчик, то уходим на перезвон
            state: ItsAnsweringMachine
                q: ($afterAutoresponder/$beforeAutoresponder)
                script: addPause();
                audio: PKU_30.03_В итоге ничего не расслышали_01.wav
                script:
                    $temp.definedEndStatus = "CALLBACK Answering machine";
                    setRedialTime();
                    log("Answering machine");
                    hangUp();

            # Если приветствие перебила просьба перезвонить позже
            state: CallBackLater
                q: * {(сейчас/общаться/*говор*/*говар*) * $later} *
                q: * {[я/мы] * (за рулем/в машине/еду (в/на/$AnyWord)/метро/поезд*/самолет*/трамва*/троллейбус*/транспорт*)} *
                q: * [сейчас] нет [сейчас] (возможнос*/время/времен*) * (говорить/разговаривать/общаться) *
                q: * [сейчас] не [сейчас] (смогу/сможем) [[$AnyWord] (говорить/разговаривать)] *
                q: * [сейчас] не [сейчас] (могу/можем) [$AnyWord] (говорить/разговаривать) *
                q: * {(я/мы) * (дороге/дороги)} *
                q: * (жду/ждем/ожидаю) [$AnyWord] [$AnyWord] (звонка/звонок) *
                q: * {времени сейчас нет} *
                q: * [давай*] * {[маленько/*много/немножко] (занят*/не вовремя/невовремя/некогда/не [очень] удобно/неудобно)} * [{$later *звони*} *]
                q: * [давай*] * (не сейчас/времени (нет/нету)) * [{$later *звони*} *]
                q: * [давай*] * ({(за ребенком) (нужн*/надо) (идти/бежать/ехать)}/{((на/в) машине) [еду/едем]}) *
                q: * [давай*] * {нельзя $communicateViaCalls} *
                q: * [давай*] * ([на] (работ*/учеб*/урок*)/на пар*/[еще] не (~встать/~проснуться)/на отдыхе/в отпуске/ребенок плачет/{(гуляю/~прогулка/гуляем) [с] ~ребенок}) * [$bye] *
                q: * [давай*/некогда] * {(позвони/позвоните/позвонить/поговорим) * $later} *
                q: * [давай*] * {(перезвони/перезвоните/перезвонить) * [$later]} *
                q: * не (могу/можем) [$AnyWord] $communicateViaCalls *
                q: * {(~перезвонить/позвони*) * $later} *
                q: * {~перезвонить * (можете/попросить)} *
                q: * в (дороге/дороги) *
                q: * (некогд*/некуда) *
                q: * давай* * через * [*час*/*минут*] *
                q: * (не (могу/можем)/нет (времени/возможности)) * $communicateViaCalls *
                q: * {(перезвон*/позвони*) * [$later]} *
                q: * [срочно* *] {времени (очень мало)} * [срочно* *]
                q: * {[я] * занят*} * $weight<+0.2>
                q: $callbackDateTime $weight<-0.1>
                q: $callback
                if: !$client.callAttempt
                    script: addPause();
                    # при последнем звонке $client.callAttempt удаляется
                    audio: PKU_30.03_Абонент отказался_01.wav
                    script:
                        $temp.definedEndStatus = "Три раза не было времени выслушать";
                        hangUp();
                else:
                    script: addPause();
                    audio: 31.03_Перезвоним позже.wav
                    script:
                        $temp.definedEndStatus = "CALLBACK";
                        setRedialTime();
                        hangUp();

            # Если приветствие перебила любая другая фраза клиента, то договариваем приветствие и ждем ответа на вопрос
            state: LocalCatchAll
                q: *
                q: $stateBargeInCatchAll
                q: * $agree *
                go!: /Start/Yes

        state: ProbablyAnsweringMachine
            event: speechNotRecognized
            q: autoanswer test
            audio: PKU_30.03_Кто звонит (до предложения)_01.wav
            script: $temp.definedEndStatus = "Клиент взял трубку и молчит";

            state: ItsAnsweringMachine
                q!: $afterAutoresponder
                q: $beforeAutoresponder || fromState = "/Start"
                q: $beforeAutoresponder || fromState = "/Hello"
                audio: PKU_30.03_В итоге ничего не расслышали_01.wav
                script:
                    $temp.definedEndStatus = "CALLBACK Answering machine";
                    setRedialTime();
                    log("Answering machine");
                    hangUp();

        state: Yes
            q: * (да) [будет] спасибо *
            # происхождение = предложение
            q: * происхождение *
            q: * {повтори* * ($service/подключ*/суть)} *
            q: * (что [значит/такое/за]/как [пон*]/[в] чем) * ($service) *
            q: * ($service) * (что [значит/такое/за]/как [пон*]/[в] чем) *
            q: *
            q: * [$unlimitedAgree] {($unlimitedAgree/уделю/{*слушаю [вас]}/как*/что/чего/какое/постара*/говори*/рассказывай*) * [раз * позвонили]} *
            q: $repeat<$unlimitedThanks>
            q: [$botName] [ну] ($agree/рога/надо/пожалуйста) [$botName] *
            q: понятно
            q: * смогу *
            q: * (говори*/давай*/да) * [*разговарива*] *
            q: * ~говорить * [что/чего] * [хотели/хотите/нужн*] *
            q: [$AnyWord] где
            q: * (занят*/некогд*) * но * ($unlimitedAgree/уделю/слушаю/как*/что/чего/какое/постара*/говори*) *
            q: * {(давай*/$yes) [только] (быстро/быстрее/не долго)} *
            q: * {хотите * предложить * [не/мне]} *
            q: * (в чем/что за) * бонус* *
            q: * время пошло *
            q: * [$agree] * {[$Number] (секунд*/минут*) * [только/макс*]} * {$available * [*говор*/разговор*/слуша*/послуша*/но]} *
            q: * {[$Number] (секунд*/минут*)} * {[только/макс*] * $available * [*говор*/разговор*/слуша*/послуша*]} *
            q: * {$available * [*говор*/разговор*/слуша*/послуша*]} * {[$Number] (секунд*/минут*)} *
            q: * {минут* * (больше/более/долее/дольше) (никак/$cannot/не смог*)} *
            q: * если (только/не более) минут* *
            q: * не (больше/более/долее/дольше) *
            q: * $agree сейчас могу *
            q: * $agree * ровно $Number минут* * $weight<+0.2>
            q: * [$agree] * не [очень] занят* *
            q: * *ближе к делу *
            q: * {(в чем/к) (~суть)} $weight<+0.2> *
            q: * {[я] (не занят*)} * $weight<+0.2>
            q: * {([ну] давай*) * ([не $weight<-1>] сейчас)  * [на улице]} * $weight<1.3>
            q: * {минут* * (уделю/уделим/$agree)} *
            q: * $firstAnswerToWelcomePhrase * $yesHaveTime || fromState = /Start/BargeIn
            q: * сейчас $can (*говорить/*говаривать) [все (орош*/$good)] *
            q: * [ну] (говори/говорите) [уже] * $weight<1.1>
            q: * ([не $weight<-1>] (хотел*/хочу/готов*/давай*)) * (*говорить/*говаривать) *
            audio: 07.10.2020_БНС_приветствие_07.10_1.wav || bargeInLabel = first
            audio: 07.10.2020_БНС_приветствие_07.10_2.wav || bargeInLabel = second
            script:
                $temp.definedEndStatus = "Предложение";
            go: /Objections/FirstStateAfterOffer

        state: WorkPhone
            q: * (корпоратив*/служебн*) (симк*/$phone/связь/номер*) *
            q: * {(~рабочий|не мой|не моя|$relationsWithoutGirlNom) * ($phone/номер*/сим*)} *
            q: * [не] (хозяин/хозяйка) $phone *
            q: * {($phone/номер*/сим*) * (зарег*|записан*|принадлежит|оформлен*) (не (на меня/мне)/на другого человека)} *
            q: * {($phone/номер*/сим*) [c|для/по] ~работа} *
            q: * {($phone/номер*/сим*) * [~такой] общий}  $weight<+0.3> *
            q: * {(мне/меня/нам/нас) * (не (*регистрирован*/записан*/принадлежит/оформлен*))} *
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Это рабочий телефон, это не мой телефон";
                hangUp();

        state: Child
            q: $stateChild $weight<+0.07>
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Ребенок";
                hangUp();

        state: IAmOld
            q: $stateOldPeople $weight<1.1>
            q: * {{я $2AnyWords [уже] (~старый/стар/стареньк*/бабушк*/дедушк*/бабк*/дед*/пенсионер*)} * [$poorEyesight/$oldPhone/$dontUnderstand/[$me] не ~разбираться]} * $weight<1.5>
            q: * {[я] [человек] [уже] (~пожилой/в возраст*/пенсионер*/[в] пенсион* возраст*)} * {телефон* * (простеньк*/стар*/кнопочн*)} *
            q: * {(~старый/~пожилой/по ~живой) ~человек} * $weight<+0.1>
            q!: $stateOldPeopleWithNoInterest
            q!: $stateOldPeople
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Пожилой/маломобильный человек";
                hangUp();

        state: ImNumberYearsOld
            q: * (мне/я) [$AnyWord] [$AnyWord] $Number (год*/лет*) *
            if: parseInt($parseTree._Number) > 60
                go!: ../IAmOld
            elseif: parseInt($parseTree._Number) < 18
                go!: ../Child
            else:
                go!: ../Yes

        state: Why
            q: * *оводче* *
            q: * $why *
            q: не слушал*
            q: * {(~какой/что * за/в чем/*мен*) * (вопрос*/повод*/цел*/$tariff/~причина)} *
            q: * что [$you] (нужно/угодно/~хотеть/хотел*/надо/интересует) *
            q: * меня (нужно/угодно/~хотеть/хотел*) *
            q: * (повтор*|(не|плохо) слыш*|не расслыш*|еще раз|что что|$what [вы/ты] сказа*|не понял*|непонятн*|{это как} понять|как как|почем) *
            q: * по * (поводу/вопросу) *
            q: * (почему/зачем) * (звонит*/разговар*) *
            q: * для чего *
            q: * {что* * (*мен*/измен*)} *
            q: * о чем * (речь/говор*) *
            q: * (на счет/насчет) (чего/того) *
            q: * [$girl] * что [то] (случилось/произошло/стряслось) * $weight<+0.15>
            audio: PKU_30.03_По_какому_вопросу_до_предложения_01.wav
            script:
                handleIfItWasNoInterest();
                $temp.definedEndStatus = "По какому вопросу до предложения";
            go: ..

        state: Suggest
            q: * [не (понимаю/понял*/допер)] * (что за/че за/чем/какая/какое/какую/какой/повтори*) * [суть/смысл] * $service *
            q: * [не (понимаю/понял*/допер)] * (повтори*/расскажите/еще раз) * [суть/смысл] * (еще/что за/че за/чем/зачем/$service/*подробнее/подробност*) *
            q: * не (понимаю/понял*/допер)
            q: * что за $service *
            q: * {можно * поподробнее * [узнать/еще раз]} * $weight<+0.3>
            q: * {объясните * (еще раз*)} *
            q: * что это * (означает/значит) *
            q: * не совсем [понял*/понима*] *
            q: * не [совсем/очень] * (понял*/понима*) *
            q: * как * будет* выглядеть *
            q: * как это будет* *
            q: * чудо памяти *
            q: * {(не понял) я} * $weight<+0.1>
            q: * {(не понял/продиктуй*/скажи*) * [пожалуйста] * (еще раз)} *
            q: * не так быстро *
            q: * ничего не понимаю *
            q: это как
            q: * ((повтор*|(не|плохо) слыш*|не расслыш*)|еще раз|что что|$what [вы/ты] сказа*|не [совсем/очень] (понял*/понима*/понятно)|непонятн*|{это как} понять|как как) *
            q: * (прослушал/в смысл*) *
            q: * ((что/какая/какое) * ($service/предла*)) *
            q: * {расскажи* * *подробн*} $weight<+0.1> *
            q: * {повтори* * ($service/подключ*/суть)} *
            q: * (что [значит/такое/за]/как [пон*]/[в] чем) * ($service) *
            q: * ($service) * (что [значит/такое/за]/как [пон*]/[в] чем) *
            q: * что * (значит/подключ*) *
            q: [$AnyWord] где
            q: * [только/но] * {(не $Set [$me]) [$2AnyWords] (друг* [$2AnyWords] (связь/тариф*/пакет*))} *
            # происхождение = предложение
            q: * происхождение *
            q: * {(хотите/хочешь) * предложить} *
            q: * {(еще раз) * повтори* } $weight<+0.1> *
            q: * $unlimitedAgreeWeak * {((что/чего) (~отказываться/~отказаться)) [что ли] (от вас)} *
            script:
                $temp.definedEndStatus = "Предложение";
            go!: /Start/Yes

        state: WhatIsYourName
            q: * {(тебя/вас/ваш*/~такой) $botName (зовут/имя/фамили*/звать)} *
            q: * {(тебя/вас/ваш*/~такой) * [мегафон*] (зовут/звать/имя/фамили*)} *
            q: * [скажи*] * (кто * (это/звонит/говор*)/представ*/кем * (говор*/разгов*/диалог)) *
            q: * {как* * (тебя/твое/вас/ваше) * (имя/зовут/звать/называ*)}
            q: * {кто (вы/ты/это) * (такие/такая/такой)} *
            q: * {[скажи*] (еще раз*/повтори*) * (имя/зовут/звать/называ*)}
            q: * (какой/какая/что за) * (компани*/компон*/фирм*) *
            q: * (откудова/откуда) * (звоните/звонишь)
            q: * {как * (звать/зовут) [тебя/вас]} *
            q: * {как * (могу/вам) * обращаться*} *
            q: * {кто (вы/ты/это)} *
            q: * (ваше/твое) имя *
            q: * {$botName * кто} *
            q: * [$girl] $botName ($questionMarkerAtTheEnd/[так] ведь)
            q: * вы [из/работ* *] [компан*] мегафон* *
            q: * это [из] [компан*] мегафон*
            script:
                handleIfItWasNoInterest();
                $temp.definedEndStatus = "Кто звонит до предложения";
            audio: PKU_30.03_Кто звонит (до предложения)_01.wav
            go: /Start

        # state: HowDidYouGetMyNumber?
        #     q: * {(как/откуда/откудова/где) * [нашли/нашла/нашел/достал*/откопа*/этот/мой/наш/вас] * (номер*/телефон*)} * || fromState = /Start, onlyThisState = true
        #     q: * откудова * || fromState = /Start, onlyThisState = true
        #     q: * {(кто/как*) * (дал*/передал*/сообщил*/отправил*) * (номер*/телефон*)} *
        #     q: * {(мой/свой) (номер*/телефон*)} *
        #     script:
        #         $temp.definedEndStatus = "Откуда номер до предложения";
        #     go: /Start

        # # Почему вы звоните с такого странного номера?
        # state: StrangeNumber
        #     q: * {(~странный/~подозрительный) ~номер [телефона]} *
        #     q: * {почему вы [звоните/позвонили] c (такого/этого) ~номер [телефона]} *
        #     script:
        #         $temp.definedEndStatus = "Странный номер до предложения";
        #     go: /Start

        state: No
            q!: * [сейчас] нет [сейчас] (возможнос*/время/времен*) * (говорить/разговаривать/общаться) *
            q!: * [сейчас] не [сейчас] (смогу/сможем) [[$AnyWord] (говорить/разговаривать)] *
            q!: * [сейчас] не [сейчас] (могу/можем) [$AnyWord] (говорить/разговаривать) *
            # я в дороге
            q!: * {(я/мы) * (дороге/дороги)} *
            q!: * (жду/ждем/ожидаю) [$AnyWord] [$AnyWord] (звонка/звонок) *
            q!: * {(я/мы) * (за рулем/в машине/еду (в/на/$AnyWord)/метро/поезд*/самолет*/трамва*/троллейбус*/транспорт*) * [~говорить]} *
            q: * {[*говар*/*говор*/*звон*] * (попозже/позже)} *
            q: * {(~перезвонить/позвони*) * (потом/*позже/позднее)} *
            q: * {~перезвонить * (можете/попросить/попрошу)} *
            q: * [попросил*] * {~перезвонить * вечер*} *
            q: * (* не будем/не/нет) $unlimitedThanks *
            q: * (не/нет) (добав*/подключ*/подключите/подключаем/добавляем/добавьте/[ну] да/подключай*/включай*/включ*/добавляй*/*авляйте/соглас*/попробую/[давай*] попробуем/давай*/хорошо/отлично/добавим/$agree/пойдет) *
            q: * $repeat<$net> *
            q: * [сейчас] {нет* [сейчас] (возможнос*/время/времен*)} *
            q: * до свидани* *
            q: * {(ничего/никакая) * (не (надо/нужн*)) * (добав*/подключ*/опци*)} *
            q: * ($disagreeStrong/не удел*/занят*/не могу/[во/не] время/не вовремя/невовремя/не сейчас/не удобно/неудобно/не за что на свете/{((на/в) машине) [еду/едем]}/{нельзя (говор*/разговарив*)}) *
            q: * ($reject/{([на] (работ*/учеб*)/на пар*) [занят*]}/[еще] не (~встать/~проснуться)/за рулем) *
            q: * (нету/нет/не [нужн*/надо/хочу/хотим]) *
            q: * давай* * (потом/*позже/позднее/другой раз) *
            q: * в движении *
            q: * спасибо *
            q: * (извини*/прости*) [но] *
            q: * (не (могу/можем)/нет (времени/возможности)) * (говорить/разговарить) *
            q: * {(перезвон*/позвони*) * [*позж*/потом]} *
            q: * (некогд*/некуда) *
            q: * (занят*/не могу [сейчас]/не вовремя/невовремя/не удобно/неудобно) *
            q: * (на (работ*/учеб*/пар*)/[еще] не (~встать/~проснуться)) *
            q: * (потом/позже/позднее/попозже) *
            q: * {(позвони/позвоните/позвонить) * (потом/позже/позднее/попозже)} *
            q: * {(перезвони/перезвоните/перезвонить) * [потом/позже/позднее/попозже]} *
            q: * [да я/$unlimitedAgree] [я/мне] (занят*/не могу/не вовремя/невовремя/не [очень/совсем] удобно/неудобно/некогда) *
            q: * руки *
            q: * [$disagree/$noCallInterest] * [я/мы] (на (работ*/учеб*/пар*)/[еще] не (~встать/~проснуться)/за рулем/в дороге/на отдыхе/в отпуске/ребенок плачет/времени (нет/нету)/{(за ребенком) (нужно/надо) (идти/бежать/ехать)}/{(гуляю/~прогулка/гуляем) [с] ~ребенок}) * $weight<+0.2>
            q: * (давай* * (*позж*/потом/вечер*/завтр*))
            q: * не * удобно (говорить/разговаривать) *
            q: * {некогда * [совсем]} *
            q: * {[говори*] (не [очень] долго/быстро/быстрее)} *
            q: * (я/меня/звоните) * ((на работе)/работ*/(на встрече)/(рабоч* ~день)/занят*) * [не до того/этого] *
            q: * мне [сейчас] не до этого *
            q: * {[я] * занят* * с детьми} * $weight<+0.3>
            q: * {[не (могу/хочу)] на улице [{ничего (не слышу)}]} *
            q: $haveNoTimeStrong || fromState = "/Hello"
            q: * {[я] * (в магазин*/на кассе/[в] метро*/тренировк*)} $weight<+0.2> *
            q: * (болею/болеем/заболел*/болен/больна/[в] (больниц*/поликлиник*)/температур*) *
            q: * {(не (могу/можем)) * [потреблять] (стою/стоим)} *
            q: * {(не (могу/можем)/я/мы) [я/мы] ([в] патрул*)} *
            q: * (другой/следующий) раз *
            q: * {($phone/аккумулятор*) * (разряжается/разрядится/садится/сядет/на (нуле/ноле))} *
            q: * {(*могу/*можем) [говорить/разговаривать] только [$AnyWord] [ран*/поздн*] (утром/днем/вечером/ночью/с утра)} *
            q: * {[я] * (занят*/под капельницей/баранку кручу/таксую/[управляю] (авто/машиной/автомобил*))} * $weight<+0.2>
            q: * {(~начальство/~начальник) [идет] * [~работа]} *
            q: * {(не [$AnyWord]) подход* (время/момент)} *
            q: $noHaveNoTime
            q: $callbackDateTime
            q: * {(*внутри/*среди/середин*) * (дня/недели) * (~позвонить/~звонить)} *
            q: * {[нет/не] * некогда [мне]} [$disconnect] *
            if: !$client.callAttempt
                # при последнем звонке $client.callAttempt удаляется
                audio: PKU_30.03_Абонент отказался_01.wav
                script:
                    $temp.definedEndStatus = "Три раза не было времени выслушать";
                    hangUp();
            else:
                audio: 31.03_Перезвоним позже.wav
                script:
                    $temp.definedEndStatus = "CALLBACK";
                    setRedialTime();
                    hangUp();

        # Вы робот
        state: YouAreRobot
            q: $YouAreRobot
            script:
                handleIfItWasNoInterest();
                $temp.definedEndStatus = "Вы робот до предложения";
            audio: PKU_30.03_Вы робот (до предложения)_01.wav
            go: /Start

        state: Anger
            q: $stateComplaint
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Абонент ругается";
                hangUp();

        # state: DontNeedAnything
        #     q: * {ничего не (нужно|надо) * [от] [вас]} *
        #     script:
        #         $temp.definedEndStatus = "Ничего не нужно";
        #     a: Я понимаю, мы все устали от рекламных звонков, но я хочу рассказать о бонусе который теперь доступен вам как нашему постоянному клиенту. Это предложение, кстати, совершенно бесплатное. Уделите мне минутку?
        #     go: /Start

    # Обработка возражений и согласий
    state: Objections

        state: FirstStateAfterOffer
        state: Agreed
            # asr подключайте
            q: (Отключайся/Отключайте)
            q: * [ну] (пускай/пусть) будет
            q: [ну/а] [попробуем] (лада/лады/ладно/наверно*/возможно/давай*) [все] *
            q: * [но/ну/[ну] давай*/да/[хорошо] (уговор*/уговар*)] {(добав*/подключ*/подключите/подключаем/добавляем/добавьте/[ну] да/подключай*/включай*/активируй*/актер/включит*/включи/включай*/добавляй*/*авляйте/соглас*/[ну] [давай*] (попробую/попробуем/проем)/давай*/хорошо/отлично/$unlimitedAgree/добавим/пойдет/делай*/возможно) * [{($later/*проб*/ну если) * [(*смотр*/отключ*/отличить/выключ*/*жале*/(будем/буду) звонить/позвоню/позвоним/подключ*) $weight<+0.1>] }/если * сдирать не (буде*/стане*)]} *
            q: [$botName] [ну] давай* [$botName]
            q: * {(если/раз) * (бесплатн*/безлимит*/не (измен*/помен*)/останется) * (подключ*/включай*/добав*/соглас*/попробу*/давай*/хорошо/можно)} *
            q: * {я [теоретически/наверное] готов} *
            q: * (можно * (~попробовать/подключить)/ну можно/воспользую*) *
            q: * (активируй*/актер/активирова*) *
            q: * ~быть * (благодар*/признат*) *
            q: * (закрепляем/закрепите/закрепим/сохраните/сохраним/оставьте/оставим) * [если [$AnyWord] что] *
            q: * (давай*/готов*) (подключай*/подключи*/попробу*/если [не]/посмотрим) $weight<+0.2> *
            q: * (добавки/забавляй*) *
            q: * полностью (согласен/согласна) *
            q: * [$AnyWord/*добр*] $unlimitedThanks * [спасибо/а то/мегафон*] *
            q: * [но] не (упущу/пущу/*пущу/упустим) * [возможность/шанс] *
            q: [$AnyWord] может быть
            q: * ($agree/рога/надо) [раз позвонили/как скаж*] *
            q: * да $repeat<$unlimitedThanks> *
            q: $repeat<$unlimitedThanks>
            q: * $AnyWord $AnyWord (то/тогда) да
            q: * [да] (да/давай*/наверн*) да
            q: [$unlimitedAgree] (активиру*/понятно) [$unlimitedAgree]
            q: * как (хочешь/хотите) [вам] [видн*] *
            q: * ($agree|добавь*) *
            q: * так * и быть *
            q: * была не была
            q: * (предостав*/представ*) *
            q: * [спасибо/пока] * (сохранит*/сохраняйте/~сохранять/оставляй*/добавляй*/будет удобно) * $weight<+0.2>
            q: * {(добавьте/давайте/подключите/добавляй*/подключай*) * хотя * (плачу вовремя/нет * проблем*)} * $weight<+0.1>
            q: * {(*оставляйте/*оставьте/активируйтe/активируем/$agree/пусть остается/попробуй*/*пробуй*/сохраняй*) * (если/только [$AnyWord/дополнительн*] чтоб*) * ($free/не * (обман*/вранье)/без (обмана/вранья)/~никакой * $service)} * $weight<+0.1>
            q: * [$agree] * {($agree/поверю/поверим/проверю/проверим/проверить) * (довер*) (нет/без/не [очень/особо/слишком])} * $weight<+0.1>
            q: * если * {(нет/без) подводн* камн*} *
            q: * я * за *
            q: * [$repeat<$unlimitedThanks>] * {((надо/нужно) быть) [$AnyWord] (сумасшедш*/душевн* больн*) * чтобы отказ*} * [$repeat<$unlimitedThanks>] *
            q: * оставьте * (что/как) * (предложили/предлагаете)  *
            q: * (подключай/подключайте/давайте/подключи*/подключаем/подключаете/вы $activate) * (если [что/не понрав*]/[в] случае чего/[в] крайнем случае) * [позвоню/напишу] * (отключ*/включ*) * $weight<+0.2>
            q: * {$agree * так * удобне*} *
            q: * (хорошо/ок) * [бывает мне заплатить/сразу * *плач* $weight<+0.2>/{приятно * слышать}]  *
            # tts сохраним
            q: (а/не) (храним/сохрани/сохраните)
            # tts включай
            q: * (чат/включай*) $weight<+0.15> *
            q: * {спасибо * удачи} $weight<+0.2> *
            q: * {[я] готов*} $weight<+0.2> *
            q: * {(хорош*/отличн*/прекрасн*/замечат*/превосходн*/классн*) * [новост*/вест*/извест*/{(тем более) * $pay (раньше/заранее)} $weight<+0.2> ]} *
            q: * {(понятн*/понял*/ясн*) * (все/это/так)} *
            q: * {($agree/активиру*) * $dontKnow $2AnyWords ($agree/активиру*)} *
            q: * $repeat<$unlimitedThanks> * {($agree/можете/активируй*) * (но/хотя/хоть/лишь бы * не/не знаю) * [$pay/измен*/помен*]} *
            q: * {$repeat<$unlimitedAgree> * [$repeat<$unlimitedThanks>/как $good $weight<1.15>]} * [посмотрим/посмотрю] *
            q: * $repeat<$unlimitedAgree> * у всех мегафон * $repeat<$unlimitedAgree> * $weight<+0.3>
            q: * (как/разве) [можно/[я/мы] (*могу/*можем)] отказ* от (так*/это*) $2AnyWords $service *
            q: * (заманчиво/{заманчивое [конечно/очень] предложение}) *
            q: * [я] * не (отказ*/откажусь/отказал*/отказываюсь) [от] * [услу*/опц*] *
            q: [$agree/ну] как скаж* *
            q: [ну] (хор/рошо/понятн*/понял*/ясн*)
            q: * {как * (удобно/угодно/удобнее)} *
            q: * (все равно/по барабану/что так что эдак/фиолетово) *
            q: * ну (попробуй*/пробуй*) $weight<+0.5> *
            q: (попробуй*/пробуй*)
            q: * ради бога $weight<+0.1> *
            q: (да/вот/ну) [$AnyWord] $perfect а то * [{$stayConnected * $emptyBalance}] * $weight<+0.3>
            q: ($agree) только чтобы [не] *
            q: * $agree * в конце концов *
            q: * (воспользу*/посмотрим/посмотрю) *
            q: * {(почему бы и (нет/не (активир*/подключ*/включ*/добав*/соглас*/попроб*))) * [$stateItsFree]} *
            q: * {((кто/(~какой/что $2AnyWords я) ($stupid $weight<+0.3>)) * ~отказаться) * [$free]} *
            q: * [давай*/да] (подключай*/подключите/активируй/подключаем/добавляем/добавь*/подключай*/включай*/добавляй*/*авляйте/*авляйте/[я] согласна/[я] согласен/попробу*/хорошо/отлично/добавим/делай*/возможно) * [если * сдирать не (будет*/станете)/$free] *
            q: * {$free * ((давай*/да) (подключай*/подключите/активируй/подключаем/добавляем/добавь*/подключай*/включай*/добавляй*/*авляйте/*авляйте/[я] согласна/[я] согласен/попробу*/хорошо/отлично/добавим/делай*/возможно))} *
            q: * {(если/раз/только [$AnyWord] чтоб*) * (бесплатн*/безлимит*/не (измен*/помен*)/останется/$free/$pay не будет) * (подключ*/включай*/добав*/соглас*/попробу*/давай*/хорошо/сделай*)} * $weight<+0.1>
            q: * (ну/давай*) {(не плох*) [это] (в принципе/$probably)} *
            q: хорошо до свидания
            q: * (кажется/думаю) [это] * (нужн*/полезн*) *  [{не [$AnyWord] [$AnyWord]} (успев*/воврем*) * $pay]
            q: * {$service *  ($good/полезн*)} * [действительно/правда] * [{не [$AnyWord]} (успев*/воврем*) * $pay] *
            q: [$AnyWord] я [$AnyWord] (понял*/услышал*) [$AnyWord]
            q: * {$good * [$good] * $repeat<$unlimitedThanks>} *
            q: [ну] * ($enable) * [не знаю] * [буду/когда] * [*польз*] *
            q: * {[$dontKnow] * (давай*/$enable)} *
            q: * (давай*/$enable/можно/можешь/можете/добавляй*) * (но/только) * [все равно] [буду] * {$pay * (ежемесяч*/кажд* ~месяц/как (всегда/обычно)/$inTime)} *
            q: * (это нормально/{это [мне/нам/прям/действит*/правда] [очень] (подойд*/подход*)}) *
            q: * [в] самый раз * [для (меня/нас)] *
            q: * [для (меня/нас)] * [в] самый раз *
            q: [$AnyWord] [$AnyWord] (пусть/пускай) {будет так} [$AnyWord]
            q: * [$yes] [$good] * если * {не [$AnyWord] (навяж*/навяз*/обман*)} *
            q: {$bye $unlimitedThanks}
            q: * [да] ($unlimitedAgreeWeak/$yes/$good/$enable) * [но/а/только] если [потом*] * $chargeBalance * $weight<+0.3>
            q: * {[$unlimitedThanks] * [$yes]} * {[меня/нас] (не $needLocal) * $disconnect} * {[$unlimitedThanks] * [$yes]} *
            q: * [хорошо] * {$unlimitedAgreeWeak * (предоставляйте/предоставьте/$enable)} * [(но/а) (если что/я мог*) [$2AnyWords] [друг* (номер*/оператор*/$tariff)/$anotherOperator]] *
            q: * $unlimitedThanks * {[уже/целых] $Number (лет/год*)} * (мегафон*/с вами) *
            q: * $unlimitedAgree * [$unlimitedThanks] * [не $weight<-1>] нравится $megafon *
            q: * {($toAnotherOperator/$megafon) * [пока] (не $goingTo) ($changeINF/$leaveINF)} *
            q: * {($toAnotherOperator/$megafon) * [пока] (не ($change01/$leave01))} *
            q: * [$unlimitedAgree] * {(хотел*/собирал*/думал*/подумывал*) (уже/было/даже) * ($leaveINF/$changeINF) ($fromMegafon/$toAnotherOperator)} *
            q: * ($unlimitedThanks/$unlimitedAgree) * ((будем/буду) (следить/стараться)/постара*/прослежу*) * [$emptyBalance] *
            q: * [$activate] * (буду/будем) [этому] [только] ~рад *
            q: * [ну $dontKnow] если можно предоставь* *
            q: * если * ({(не (буду/будем)) * $pay}/$free) * ($agree/$activate) *
            if: $session.lastState === "/Objections/Agreed/IsAgreed" && !$temp.repeat
                go!: ./IsAgreed/DefinitelyAgree
            if: $session.proposalInterruption
                audio: Повтор условий Согласие_на_повтор.wav
            else:
                script:
                    $session.wasInFirstAgree = true;
                    manageRepeatsAndAnswer([
                        "PKU_30.03_Согласие на повтор_01.wav",
                        "PKU_30.03_Согласие на повтор-1_01.wav",
                        "PKU_30.03_Согласие на повтор-2_01.wav"
                    ]);
                go!: ./IsAgreed

            state: IsAgreed
                script:
                    $temp.definedEndStatus = "Ждем уточнения согласия";

                state: DefinitelyAgree
                    q: $weight<+0.4> $instantAgreeAfterOffer || fromState = /Objections/FirstStateAfterOffer
                    q: ($agree|правильно) || fromState = /Objections/Agreed
                    q: $weight<+0.4> $instantAgreeBySeveralWords || fromState = /Objections
                    q: * {$unlimitedAgree * спасибо * предостав*} * || fromState = /Objections
                    q: * {$repeat<$agree> * ((что/зачем/почему) * отказываться) * [($free/{списывать * не будете}/ничего не стоит)]} * $weight<+0.2> || fromState = /Objections
                    q: $instantAgreeWord || fromState = /Objections

                    q: * [давай*/да] (подключай*/подключите/активируй/подключаем/добавляем/добавь*/подключай*/включай*/добавляй*/*авляйте/*авляйте/согласна/согласен/попробу*/хорошо/отлично/добавим/делай*/возможно) * [если * сдирать не (будет*/станете)] *
                    q: * (подожди*/обожди*/минуточк*/подож*/погоди*) $weight<-1> *
                    q: [ну] (лада/лады/наверно*) *
                    q: * [но/давай*/ну] (добав*/подключ*/подключите/подключаем/добавляем/добавьте/[ну] да/подключай*/включай*/активируй*/актер/включ*/добавляй*/*авляйте/посмотрим/соглас*/попробую/[давай*] попробуем/давай*/хорошо/отлично/добавим/$unlimitedAgree/пойдет) * [{потом * [*смотр*/отключ*/отличить/выключ*/*жале*]}/не понравится/$dontKnow] *
                    q: * {(если/раз/только [$AnyWord] чтоб*) * ($free/безлимит*/не (измен*/помен*)/останется) * (подключ*/включай*/добав*/соглас*/попробу*/давай*/хорошо)} *
                    q: * {я [теоретически/наверное] готов} *
                    q: * (можно ~попробовать/ну можно/воспользую*) *
                    q: * (активируй*/актер/активирова*) *
                    q: * ~быть * (благодар*/признат*) *
                    q: * давай* (подключай*/подключи*/попробу*) *
                    q: * (добавки/забавляй*) *
                    q: * полностью (согласен/согласна) *
                    q: * правильно *
                    q: * готов* *
                    q: * {предостав* * [$unlimitedAgree] * [$thanks]} *
                    q: * {(уже/только) * сказал*} *

                    q: [$botName] [ну] $unlimitedAgree [$botName]
                    q: [$botName] [ну] $thanks [$botName]
                    q: [$botName] [ну] давай* [$botName]
                    q: * не против *
                    q: * (добав*/активируй*/актер/активирова*/забавляй*) *
                    q: * ($agree/рога/надо) * [$repeat<$unlimitedThanks>/как скаж*] *
                    q: * хорошо * [$repeat<$unlimitedThanks>] *
                    q: $repeat<$unlimitedThanks>
                    q: * против не имею *
                    q: * [конечно] воспользуюсь *
                    q: * ([ну] да/подключ*/включ*/добав*/соглас*/[давай*] попробу*/давай*/хорошо/отлично/добавим/$unlimitedAgree/пойдет) *
                    q: * давай* (подключай*/подключите/подключаем/добавляем/добавь*/подключай*/включай*/добавляй*/*авляйте/согласна/согласен/попробу*/хорошо/отлично/добавим) *
                    q: * (подключай*/подключите/подключаем/добавляем/добавь*/подключай*/включай*/добавляй*/*авляйте/согласна/согласен/попробу*/хорошо/отлично/добавим) *
                    q: * {(если/раз) * ($free/безлимит*) * (подключ*/включ*/добав*/соглас*/попробу*/давай*/хорошо)} *
                    q: * (можно ~попробовать/ну можно) *
                    q: * не (упущу/пущу/*пущу/упустим) * [возможность/шанс] *
                    q: * (давай*/готов*) (подключай*/подключи*/попробу*) $weight<+0.3> *
                    q: * {$agree * подводн* камн* (нет/без)} *
                    q: * ~быть * (благодар*/признат*) *
                    q: * [спасибо] * не откажусь *
                    q: [$AnyWord] может быть
                    q: [$AnyWord] $repeat<$da> [$AnyWord]
                    q: * $AnyWord $AnyWord (то/тогда) да
                    q: * [да] (да/давай*/наверн*) да
                    q: [$unlimitedAgree] активиру* [$unlimitedAgree]
                    q: * [спасибо] * (сохранит*/сохраняйте/~сохранять/оставляй*) * $weight<+0.1>
                    q: [девушка/ол*/да*/соглас*/добав*/актив*] [я] [уже/же] ((сказал*/говорил*/говорю) [давай*/что] (да/подключ*/активаци*/активируй* [активируй*]/добав*)/соглас*/попробу*) [давай*] || fromState = /Objections
                    q: [девушка/ол*] [давай*] (да [да] [да]/подключ* [подключ*]/активаци*/активируй* [активируй*]/добав*/соглас*/попробу*) [давай*] || fromState = /Objections
                    q: * [я] * не (отказ*/откажусь/отказал*/отказываюсь) [от] * [~такой] [услу*/опц*] *
                    q: * {$repeat<$unlimitedAgree> * [$repeat<$unlimitedThanks>/как $good]} * [посмотрим/посмотрю] *
                    q: [$agree/ну] как скаж* *
                    q: * (воспользу*/посмотрим/посмотрю) *
                    q: * (кажется/думаю) [это] * (нужн*/полезн*) *
                    q: * {$service *  $good} * [действительно/правда] * [{не [$AnyWord]} (успев*/воврем*) * $pay] *
                    q: [$AnyWord] я [$AnyWord] (понял*/услышал*) [$AnyWord]
                    q: хорошо до свидания
                    q: [ну] * ($enable) * [не знаю] * [буду/когда] * [*польз*] *
                    q: * {[$dontKnow] * (давай*/$enable)} *
                    q: * (давай*/$enable/можно/можешь/можете) * но * {$pay * (ежемесяч*/кажд* ~месяц/как (всегда/обычно))} *
                    q: * (это нормально/{это [мне/нам/прям/действит*/правда] [очень] (подойд*/подход*)}) *
                    q: * [в] самый раз * [для (меня/нас)] *
                    q: * [для (меня/нас)] * [в] самый раз *
                    script:
                        $temp.definedEndStatus = "ALMOST__ACCEPTED";
                        manageRepeatsAndAnswer([
                            "PKU_22.06_быть_на_связи_заключит_после_согласия.wav"
                            // новый год 2021 "14.12_БНС_после_согласия.wav"
                        ]);
                    go!: /AlmostAccepted

                state: No
                    q: * (нет/не) * [но/давай*] (добав*/подключ*/подключите/подключаем/добавляем/добавьте/[ну] да/подключай*/включай*/актер/активируй*/включ*/добавляй*/*авляйте/соглас*/попробую/[давай*] попробуем/давай*/хорошо/отлично/добавим/$unlimitedAgree/пойдет/боги/предостав*) *
                    q: * { ( (не/нет) (соглас*/хочу/добав*/соглаш*/добав*/добавляй*/*авляйте/подключ*/нужн*/надо/будем/попроб*/интересно/интересует/воспользуюсь)) * ($Number [рубл*/руб/р/рэ]) * ([в/за] ~месяц) } *
                    # ASR "не надо" -> "мне надо"
                    q: * {(не надо) подключ*} *
                    q: * да нет *
                    q: * не вижу смысл* *
                    q: * нет * нет *
                    q: мне интересно *
                    q: * через *
                    q: * нафига [мне] * {[$service] [за] * [р*] * [сутк*]} *
                    q: * (воздержусь/воздержимся/выпускаю/*тавляйте) *
                    q: * да ничего * $weight<+0.1>
                    q: * мне надо * (добавлять/активировать) *
                    q: * не $weight<+0.05> *
                    q: * не буд* $weight<+0.1> *
                    #q: * (подожди*/обожди*/минуточк*/подож*) $weight<+0.3> * || fromState = /Objections/Agreed
                    q: * {(спасибо/нет) * (не (надо/нужн*/добав*/подключ*)/нет/свидания/пока)} *
                    q: * {($service/абцис*/Алис*) * (не (нужн*/сдал*/всрал*/подлюч*))} *
                    q: * спасибо * (но/нет/не) * [спасибо]
                    q: * (еще раз/повтор*) * (нет/не) *
                    q: * (говор*/не понимае*) * [мне] (не (надо/нужн*)) *
                    q: * дополнительн* * (не (надо/нужн*)) *
                    q: * (не (надо/нужн*)) * дополнительн* *
                    q: * [$disagree] * не (нужн*|надо|готов*|подключа*) *
                    q: * [точно/пожалуй] * $no * [пожалуй]
                    q: * {(ничего/никакая) * (не (надо/нужн*)) * (добав*/подключ*/$service)} *
                    q: * (не/нет) * (соглас*/соглаш*/добав*/активируй*/актер/активирова*/добавляй*/*авляйте/подключ*/нужн*/надо/будем/попроб*/добавки/интересно/интересует/воспользуюсь/забавляй*) * [ничего] *
                    q: * (не/нет) * (подключай*/подключите/подключаем/добавляем/добавь*/подключай*/включай*/добавляй*/*авляйте/согласна/согласен/попробу*/хорошо/отлично/добавим) *
                    q: [$botName/девушка] [ну] (не/нет) (($agree/рога/надо)/хочу/хотим/надо/$unlimitedAgree) [$botName/девушка]
                    q: * (упущу/пущу/*пущу/упустим) * [возможность/шанс] *
                    q: * [я] * (отказ*/откажусь/отказал*/отказываюсь) [от] * [~такой] [услу*/опц*] *
                    q: * передумал* * || fromState = "Objections/Agreed/IsAgreed", onlyThisState = true
                    q: * (не (надо/нужн*) * (добав*/подключ*/$service/платеж)) *
                    q: * {ничего * (добав*/подключ*)} *
                    q: * {спасибо * (не (надо/нужн*/добав*/подключ*))} *
                    q: * {(не буд*) * (подключ*/добав*)} *
                    q: * не могу *
                    q: * {(соглас*/конечно) (не/нет)} $weight<+0.3> *
                    q: * [да] [нет] * (нет/не [надо/нужно]) * $repeat<$unlimitedThanks> [большое] *
                    q: * [да] $repeat<$unlimitedThanks> [большое] [но] $2AnyWords (нет/не) *
                    q: * не (актуально/интересно) *
                    q: * ни в коем случае *
                    q: * не пойдет *
                    q: * не надо *
                    q: * (не могу/нету) *
                    q: * (запреща*/не разреша*/против*) *
                    q: * [$botName/девушка] [ну] не включ* [$botName/девушка] *
                    q: * {[я/мы] против} *
                    q: * [$probably] [$botName/девушка] [ну] не (($agree/рога/надо)/$unlimitedAgree) [$botName/девушка] *
                    q: * [$botName/девушка] [ну] не (скажу/хочу говорить) [$botName/девушка] * || fromState = "Objections/No/FirstTime", onlyThisState = true
                    q: * [$disagree] * не (нужн*|надо|подключа*) *
                    q: * [$disagree] * не готов* $weight<+0.2> *
                    q: * [точно/пожалуй] * $no * [пожалуй] *
                    q: * {($disagree/не (надо/нужно/стоит)) * (предоставлять/подключать/добавлять)} *
                    q: * {обойдусь * [без * $service]} *
                    q: * {не нужен * (сервис/бонус)} * $weight<+0.2>
                    q: * но нет * $weight<+0.4>
                    q: * {$good * без * $service} *
                    q: * никуда ничего *
                    q: * {(сколько [$AnyWord] раз [$AnyWord] (повтор*/говорить/сказать)) * ($disagree/не надо/не нуж*)}
                    q: * ($disagree/все) * $bye *
                    q: * {$unlimitedAgree * спасибо * (предостав* кому (то/нибудь) другому)} *
                    go!: /Objections/No

        state: No
            q: * (нет/не) * [но/давай*] (добав*/подключ*/подключите/подключаем/добавляем/добавьте/[ну] да/подключай*/включай*/актер/активируй*/включ*/добавляй*/*авляйте/соглас*/попробую/[давай*] попробуем/давай*/хорошо/отлично/добавим/$unlimitedAgree/пойдет/боги/предостав*/жела*) *
            q: * { ( (не/нет) (соглас*/хочу/добав*/соглаш*/добав*/добавляй*/*авляйте/подключ*/нужн*/надо/будем/попроб*/интересно/интересует/воспользуюсь)) * ($Number [рубл*/руб/р/рэ]) * ([в/за] ~месяц) } *

            q: * {(не надо) подключ*} *
            q: * да нет *
            q: * [$disagree] * {(нет/не $activate) * пока} * $weight<+0.2>
            q: * не вижу смысл* *
            q: * нет * нет *
            q: мне интересно *
            q: * нафига [мне] * {[$service] [за] * [р*] * [сутк*]} *
            q: * (воздержусь/воздержимся/выпускаю/*тавляйте) *
            q: * нет * и все
            q: * (да/надо) ничего *
            q: * {(не (буд*/собираюсь/нужд*)) * [$disagree]} *
            q: * {($service/абцис*/Алис*) * (не (нужн*/сдал*/всрал*/подлюч*))} $weight<+0.25> *
            q: * {(спасибо/нет) * (не (надо/нужн*/добав*/подключ*)/нет)} *
            q: * спасибо * (но/нет/не) * [спасибо]
            q: * (говор*/не понимае*) * [мне] (не (надо/нужн*)) *
            q: * дополнительн* * (не (надо/нужн*)) *
            q: * {(соглас*/конечно) (не/нет)} $weight<+0.2> *
            q: * (не (надо/нужн*)) * дополнительн* *
            q: * [$probably] [уже есть] [мне] [$disagree/$repeat<$unlimitedThanks>] * [мне] не (нужн*|надо|готов*|подключа*|воспользую*|добавляй*|устраивает/включ*/стоит/пойдет/нуждаюсь/~сохранять $weight<+0.3>) * [(нечего/не (надо/нужно/стоит)) (звонить/названивать)] *
            q: * (не/нет) * (нужн*|надо|нужда*|добавки|готов*|подключа*|воспользую*|добавляй*|устраивает/включ*/пойдет/добав*/активируй*/актер/активирова*) *
            q: * [$repeat<$unlimitedThanks>] * (упущу/пущу/*пущу/упустим) * [возможность/шанс] *
            q: * [точно] * $no *
            q: * [$dontKnow] * {[$repeat<$unlimitedThanks>] * (не (соглас*/соглаш*/$agree/рога/хочу/хотим/добав*/добавляй*/*авляйте/подключ*/нужн*/надо/будем/попроб*/сохраняй*/сохраняю))} * [ничего/потому что/слишком] * [{$no $internet}] *
            q: * [я/$unlimitedThanks] * {(отказ*/откажусь/отказал*/отказываюсь) [вообще]} [от] * [~такой] [услу*/опц*/$unlimitedThanks] *
            q: * передумал* * || fromState = /Objections/Agreed/IsAgreed, onlyThisState = true
            q: * {(ничего/никакая) * (не (надо/нужн*)) * (добав*/подключ*/$service)} *
            q: * [категорическ*] нет *
            q: * добр* не (дал*/даю) *
            q: * [да] [нет] * (нет/не) * $repeat<$unlimitedThanks> [большое] *
            q: * [да] $repeat<$unlimitedThanks> [большое] [но] $2AnyWords (нет/не) *
            q: * [$repeat<$unlimitedThanks>] * (не (надо/нужн*) * (добав*/подключ*/$service/платеж)) *
            q: * [$repeat<$unlimitedThanks>] * (не/нет) (добав*/подключ*/подключите/подключаем/добавляем/добавьте/[ну] да/подключай*/включай*/включ*/добавляй*/*авляйте/соглас*/попробую/[давай*] попробуем/давай*/хорошо/отлично/добавим/$unlimitedAgree/хочу/пойдет/активируй*/актер/забавляй*/хочешь/хочется) *
            q: * не (актуальн*/интерес*) *
            q: * ни в коем случае *
            q: * (не могу/нету)*
            q: * не пойдет *
            q: * (запреща*/не разреша*/против*) *
            q: * [$repeat<$unlimitedThanks>] * {ничего * (добав*/подключ*)} *
            q: * [$repeat<$unlimitedThanks>] * {спасибо * (не (надо/нужн*/добав*/подключ*))} *
            q: * [$repeat<$unlimitedThanks>] * {(не буд*) * (подключ*/добав*)} *
            q: * не могу *
            q: * $repeat<$net> *
            q: * [я/мы] [не $weight<-0.5>] против $weight<+0.5> * || fromState = /Objections
            q: * да нет [наверное] *
            q: * давай* (* не будем/не/нет) *
            q: * (не/плохо) (понимае*/слыш*) * [сказал*] * (нет/отказ*/не) * [не нравится] *
            q: * (* не будем/не/нет) спасибо *
            q: * (попробуй/попробуйте) [ты] (себе/сами*) *
            q: * (не/ничего) * (закрепляем/закрепите/закрепим/~закреплять/сохраните/сохраним/оставьте/оставим/~сохранять) *
            q: * сколько раз * сказать нет *
            q: * [думаю] [что] * не (нужн*/нужен) [ничего] *
            # tts пока нет
            q: * [давайте] конец *
            q: * ($disagree/все) * $bye * $weight<+0.2>
            q: * {($disagree/не (надо/нужно/стоит)) * (предоставлять/подключать/добавлять) * [услуг*/опци*]} *
            q: * не стоит *
            q: * {обойдусь * [без * $service]} *
            q: * {не нужен * (сервис/бонус)} * $weight<+0.2>
            q: * но нет * $weight<+0.4>
            q: * {не (нужн*|надо|готов*|подключа*|воспользую*|добавляй*|устраивает/включ*/пойдет/нуждаюсь/активир*) * (так*/никак*) * $service} * $weight<+0.2>
            q: * некогда * минус * пополн* *
            q: * никуда ничего *
            q: * {(сколько [$AnyWord] раз [$AnyWord] (повтор*/говорить/сказать)) * $disagree}
            q: * не (особо/больно) пользуюсь *
            q: * {[давайте] $AnyWord $disagree ($dontKnow/[пока/лучше] не (буду/будем/оставим/стоит))} *
            q: * $disagree $repeat<$unlimitedThanks> *
            q: * $repeat<$unlimitedThanks> $disagreeStrong *
            q: * $repeat<$disagree> * не [$AnyWord] добавляй* * $weight<+0.1>
            q: * {[не] (хочу/мне бы/хотелось/хотел*) * [как можно] (*меньше/минимум*/никак*) * $service} *
            q: * {не хочу * [как можно] подключ* * $service} *
            q: * {не* не* [спасибо $weight<+0.2>] [спасибо $weight<+0.2>] } *
            q: * {(не актуальн*) * информаци*} $weight<+0.2> *
            q: * {(не буду) * делать * ничего*} $weight<+0.2> *
            q: * {(не (*надо*/нужн*)) * (ничего/никто) (претензи*) * (нет/нету/отсутствую*)}  $weight<+0.4> *
            q: * {(не (постоянно/всегда)/редко) * (*польз*) * [работ*/рабоч*]} * $weight<+0.1>
            q: * {$repeat<$disagree> * можно отключить} * $weight<+0.3>
            q: * [потому что] [я] консерват* *
            q: * $disagree * буд* $money полож* $2AnyWords не буд* {(разговаривать/говорить/пользоваться) не буд*} * [$disagree] *
            q: * {(предлагаете/предлагаешь/втюхиваете/втюхиваешь) * [как* [то/нибудь]] * (ерунд*/фигн*/хрен*)} *
            q: * {(*достаточн*/хватает) * $money} *
            q: * если * {$money * нет} * [то/тогда] * (нет/не (надо/нужно)) *
            q: * {((ничего/много/нога/почти/практически/особо) * не/не много/немного/мало) $2AnyWords (звоню/смотрю/пользуюсь/использую)} *
            q: * {(давай* [лучше]/лучше [давай*]/сохранять) $2AnyWords не (надо/нужно/буд*/стоит)} *
            q: * {(ничего [никогда/нигде] не подключаю) * $repeat<$disagree>} *
            q: * {[все] $bye [девушка]}
            q: * {$unlimitedAgree * [$unlimitedThanks/лучше] * (предостав*/подключит*/добав*/активир*) (кому (то/нибудь) другому)} * $weight<+0.1>
            q: * {([ваш] $bot) * (не нравится) * [как [он*] работает]} * $weight<+0.1>
            q: * {(редко/не часто) (пользу*/использу*) * $megafon} * $weight<+0.2>
            q: * {(не (пользу*/использу*)) * $megafon} * $weight<+0.2>
            q: * [$disagree] * [лучше/давай*] * {($turnOff/~делать) * [если] * [$emptyBalance/0] [как обычно]} *
            q: * пожалуй нет *
            q: * {не (просил*/нужн*/надо*) * [этого/ничего]} * (делать/$enable) *
            q: * (делать/$enable) * {не (просил*/нужн*/надо*) * [этого/ничего]} *
            q: * {не (надо/нужн*) ($free/без оплат*)} *
            q: * {[мне/для меня] * (ни к чему) * [это]} *
            q: * {((должен/хочу/хотелось [бы]/надо/нужно) (знать/понимать/быть в курсе)) * когда ($pay/(заканч*/оканч*/оконч*/нач*) [$AnyWord] [пероид*/срок*])} *
            q: * {[но] * [предлож*/~бонус/~опция] * не (понрав*/нравит*)} *
            q: * {не (польз*/использ*) * [дополн*/подключ*/ваш*]} * {$service * [$2AnyWords] [вообще/совсем/никогда] } *
            q: * {(~нуль/~ноль/0) * (отключай*/отключи/отключите/выключай*/выключите/отрубай*/отрубите)} *
            q: * {не (думаю/уверен*) *  что * (надо/нужно/хочу/хотим)} *
            q: * не *польз* * {(мобильн*/телефон*) $internet} *
            q: * {(мобильн*/телефон*) $internet} *  не *польз* *
            q: * {$money * $rechargeBalance} * {(тут/поблизость/нигде/негде/у нас/в наше) * (нигде/негде)} *
            q: * $dontKnow * {не (надо/нужно/~хотеть) *  ничего} *
            q: * {[нам/мне] * не (надо/нужно)} *
            q: * $repeat<$no>* * (оставь*/верни*/не $enable) * [$rechargeBalance * (день/~момент/~время)] *
            q: * $repeat<$no>* * (оставь*/верни*/не $enable) * ($DateHoliday/(23/двадцать треть*) ~февраль/~восьмое март*) * [$repeat<$unlimitedThanks>] *
            q: [да] (можно/может) [$AnyWord] не (нужно/надо/стоит/будем/станем)
            q: * {$repeat<$no> *  $unlimitedThanks * [не (надо/нужно)]} *
            q: * {(не (надо/нужно)) * [ничего] * ($enable/предост*)} *
            q: * ($no/нету) * {ничего * (не (нуж*/надо))} *
            q: * [это] уже смешно * $bye *
            q: * {({(~убрать*/не (включай*/подключай*)) * (~функция/$service)}/не (включай*/подключай*)) * (отключал*/отключен*/убирал*/убран*/удалял*/удален*/удалил*)} *
            q: * {((~мой/~старый/~текущ*/нынешн*/настоящ*/у меня) $tariff) * $you * ([уже] не (~устраивать/~устроить/~нравиться/удовлетвор*))} *
            q: * {{[лучше] [вообще] * $reject} * {(не (пользуюсь/использую/$needLocal)) * [$lk]} * [(только/всего лишь/просто) *звон*]} *
            q: * у меня * $mobileOperator *
            q: * { ((не/нет) (соглас*/хочу/хотелось/добав*/соглаш*/добав*/добавляй*/*авляйте/подключ*/нужн*/надо/будем/попроб*/интересно/интересует/воспользуюсь)) * $noReason * [минус* тариф*]} *
            q: * {$anotherOperator * ([у] (меня/нас)/есть/*пользу*) (уже/давно/дольше)} *
            q: * ($no/$disagree) * {специально * ($turnOff/убрал*/убирал*)} *
            q: * [не $need] * не (нужен/нужн*) * обещанн* платеж* *[ничего] *
            q: * $disagree * (ты/вы) {[меня/нас] [что [ли]] (не слыш*)} *
            q: * [я/мы] (понимаю/понимаем) * все равно $disagree *
            q: $stateDisagreeWeak
            q: * {(оплачу/оплатим/заплачу/заплатим) * (когда [будет] (нужн*/надо))} *
            q: * {($megafon/вам) (не (доверяю/доверяем))} *
            script:
                handleNo();
            if: $session.timesOfNo === 1
                go!: ./FirstTime
            elseif: $session.timesOfNo === 2
                go!: ./SecondTime
            else:
                go!: ./LastTime

            state: FirstTime
                script:
                    $temp.definedEndStatus = "Первый отказ";
                if: $session.refuseInterruption
                    if: $session.refused === 1
                        audio: Перебивание возражением.wav
                    else:
                        audio: Перебивание возражением 2.wav
                    script: $session.refuseInterruption = false;
                else:
                    audio: Общее возражение 1_UPD_01.06.wav
                # audio: 16.06_PKU_Общее возражение 1_01.wav
                # audio: 2.10.2020_PKU_Общее_возражение_1_БНС_от_02.10.wav

            state: SecondTime
                script:
                    $temp.definedEndStatus = "Второй отказ";
                if: $session.refuseInterruption
                    if: $session.refused === 1
                        audio: Перебивание возражением.wav
                    else:
                        audio: Перебивание возражением 2.wav
                    script: $session.refuseInterruption = false;
                else:
                    # audio: PKU_30.03_Общее возражение 2.wav
                    #audio: 23.12_БНС. Общее возражение 2.wav
                    audio: 08.04.21_Общее возражение 2.wav

            state: LastTime
                audio: PKU_30.03_Абонент отказался_01.wav
                script:
                    $temp.definedEndStatus = "Последний общий отказ";
                    hangUp();

        state: WorkPhone
            q: * (корпоратив*/служебн*) (симк*/$phone/связь/номер*) *
            q: * {(~рабочий|не мой|не моя|$relationsWithoutGirlNom) * ($phone/номер*/сим*)} *
            q: * [не] (хозяин/хозяйка) $phone *
            q: * {($phone/номер*/сим*) * (зарег*|записан*|принадлежит|оформлен*) (не (на меня/мне)/на другого человека)} *
            q: * {($phone/номер*/сим*) [c|для/по] ~работа} *
            q: * {($phone/номер*/сим*) * [~такой] общий}  $weight<+0.3> *
            q: * {(мне/меня) * (не (*регистрирован*/записан*/принадлежит/оформлен*))} *
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Это рабочий телефон, это не мой телефон";
                hangUp();

        state: IAmOld
            q: $stateOldPeopleWithNoInterest
            q: $stateOldPeople $weight<1.1>
            q: * {{я $2AnyWords [уже] (~старый/стар/стареньк*/бабушк*/дедушк*/бабк*/дед*/пенсионер*)} * [$poorEyesight/$oldPhone/$dontUnderstand/[$me] не ~разбираться]} * $weight<1.5>
            q: * {[я] [человек] [уже] (~пожилой/в возраст*/пенсионер*/[в] пенсион* возраст*)} * {телефон* * (простеньк*/стар*/кнопочн*)} *
            q: * {(~старый/~пожилой/по ~живой) ~человек} * $weight<+0.1>
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Пожилой/маломобильный человек";
                hangUp();

        state: ImNumberYearsOld
            q: * (мне/я) [$AnyWord] [$AnyWord] $Number (год*/лет*) *
            if: parseInt($parseTree._Number) > 60
                go!: ../IAmOld
            elseif: parseInt($parseTree._Number) < 18
                go!: ../Child
            else:
                go!: /CatchAll

        # У меня старый телефон
        state: OldPhone
            q: * {(~старый/~древний/кнопочн*/~старенький) $phone} * $weight<+0.2>
            q: * (не смартфон*) *
            q: * {(простой/простенький/кнопочн*/кнопка*/~старенький) * $phone} * $weight<+0.2>
            script:
                $temp.definedEndStatus = "Старый телефон";
                manageRepeatsAndAnswer([
                    "PKU_30.03_У меня старый телефон_01.wav"
                ], true);

        state: Suggest
            q: * [не (понимаю/понял*/допер)] * (что за/че за/чем/какая/какое/какую/какой/повтори*) * [суть/смысл] * $service *
            q: * [не (понимаю/понял*/допер)] * (повтори*/расскажите/еще раз) * [суть/смысл] * (еще/что за/че за/чем/зачем/$service/*подробнее/подробност*) *
            q: * не (понимаю/понял*/допер)
            q: * что за $service *
            q: * {можно * поподробнее * [узнать/еще раз]} * $weight<+0.3>
            q: * [давай*/мож*] * [$enable/попроб*] * {можно * поподробнее * [узнать/еще раз]} * $weight<+0.3> || fromState = /Objections/Agreed
            q: * {объясните * (еще раз*)} *
            q: * что это * (означает/значит) *
            q: * не совсем [понял*/понима*] *
            q: * не [совсем/очень] * (понял*/понима*) *
            q: * как * будет* выглядеть *
            q: * как это будет* *
            q: * чудо памяти *
            q: * {(не понял) я} * $weight<+0.1>
            q: * {(не понял/продиктуй*/скажи*) * [пожалуйста] * (еще раз)} *
            q: * не так быстро *
            q: * ничего не понимаю *
            q: * {[вообще/совсем] ничего (не (понял*/понимаю))} *
            q: * {(вообще/совсем) [ничего] (не (понял*/понимаю))} *
            q: * что [то] * не (понял*/понятн*) *
            q: * [[по] как*] вопрос* *
            q: это как
            q: * ((повтор*|(не|плохо) слыш*|не [все] расслыш*|не [все] услыш*)|еще раз|что что|$what [вы/ты] сказа*|не [совсем/очень] (понял*/понима*/понятно)|непонятн*|{это как} понять|как как) *
            q: * (прослушал/в смысл*) *
            q: * [не понял*] * ((что/какая/какое) * ($service/предла*)) *
            q: * {расскажи* * *подробн*} $weight<+0.1> *
            q: * {повтори* * ($service/подключ*/суть)} *
            q: * (что [значит/такое/за]/как [пон*]/[в] чем/как*) * ($service) *
            q: * ($service) * (что [значит/такое/за]/как [пон*]/[в] чем) *
            q: * (какую/как) * || fromState = /Objections/Agreed
            q: * что * (значит/подключ*) $weight<+0.3> * || fromState = /Objections/Agreed
            q: * что * (значит/подключ*) *
            q: [$AnyWord] где
            # происхождение = предложение
            q: * происхождение *
            q: * (это|предлагаете) * отсроч* [$pay] *
            q: * {(могу|можно) * ($pay|пополн*) * (позже|когда * (угодно|удобно))} *
            q: * {(можно|*могу) * (общаться|пользоваться|звонить|разговаривать) * при (нол*|нул*|минус*)} * $weight<+0.1>
            q: * [то есть] * [до] (какого|(двадцать/дадцать) шесто*) числ* *
            q: * {платить * (двадцать/дадцать) шестого} *
            q: * {(что/кого/чего) * (подключ*/предостав*)} *
            q: * {[что] * (хотите/хочешь/думаете) * (предложить/предоставить/подключить)} *
            q: * если * (нол*/нул*) * (будут/будет*) * деньги *
            q: * {когда * (пополнять/платить/класть/ложить/оплачивать)} *
            # q: * {(ничего/ниче) * (не понял/не понятно) * [вообще/ваще]} $weight<+0.2> * || fromState = "/AlmostAccepted", onlyThisState = true
            # q: * (повтори*/3/не *слышал*/[не] *слушал*/еще раз) $weight<+0.2> * || fromState = "/AlmostAccepted", onlyThisState = true
            # q: * {(что/чего/че/чо) * [вы] * (подключ*/активир*/добав*/включи*)} $weight<+0.2> * || fromState = "/AlmostAccepted", onlyThisState = true
            # q: * (в [каком] смысле/подожди*) * || fromState = "/AlmostAccepted", onlyThisState = true
            # q: * это $free * || fromState = "/AlmostAccepted", onlyThisState = true
            q: * {(что) * (спрашив*/говори*) [не (понимаю/пойму)]} *
            q: * {(еще раз) * повтори* } $weight<+0.3> *
            q: * о чем * (речь/говор*) *
            q: * до $Number [числ*] * (нужно/надо) $pay * [чтобы * не*] * [$emptyBalance] *
            q: * минуточк* *
            q: * это {$service * (будет/дополнительн*)} * [получается/выходит/видимо] *
            q: * {(в смысле/то есть/получается) * $enable * что} *
            q: * {(повтори*/повторить) * [пожалуйста/можете/можно/могли]} *
            script:
                $temp.definedEndStatus = "По какому вопросу после предложения";
                $session.questionAsked = true;
            # audio: PKU_30.03_Предложение повтор.wav
            audio: 16.06_PKU_Предложение повтор_1.wav || bargeInLabel = "first"
            audio: 16.06_PKU_Предложение повтор_2.wav || bargeInLabel = "second"

        state: ActivateToAnotherPerson
            q: * {(можно/может/давай*/как то/могу/чтобы) * $activateToAnother * (~парень/девушк*/~папа/~мама/~родители/~муж/~жена/~брат/~сестра/~сын/~дочь/~внук/~внучка/друго* [человек*]/~другой номер*)} *
            q: * $activateToAnother * (~парень/девушк*/~папа/~мама/~родители/~муж/~жена/~брат/~сестра/~сын/~дочь/~внук/~внучка/друг* [человек*]/~другой номер*) *
            q: * {($activateToAnother/можно) * ((друг*/[на] еще [на] ~один) $2AnyWords (номер*/$phone/устройств*))} *
            q: * (можно/можете) $activateToAnother $AnyWord (маме/папе/родителям) $AnyWord (на [его/ее/их/другой] (номер/телефон)) * $weight<+0.2>
            q: * {$activateToAnother * {только [на] $AnyWord ~этот $phone} * $can} *
            script:
                $temp.definedEndStatus = "Подключить другому человеку";
                $session.questionAsked = true;
            audio: PKU_30.03_Могу_подключить_другому_человеку_01.wav

        state: WhenWillBeActivated
            q: * {(когда/во сколько/время/как скоро/как (скоро/быстро)) * (подключ*/активирова*/активир*/добав*/включ*/станет/*пользовать*/*пользу*/доступен*/~действовать/начнет*/начинает*/предостав*) * [$service/это/$unlimited]} *
            q: * {(сразу/[с] сегодн* [~день]) (подключ*/будет) [$service/$unlimited]} *
            q: * {(через сутки/телеспутник) подключ* [$service/$unlimited]} *
            script:
                $temp.definedEndStatus = "Когда произойдет подключение";
                $session.questionAsked = true;
            audio: PKU_30.03_Когда произойдет подключение_01.wav

        state: StopSpam
            q: $stopSpam
            script:
                $temp.definedEndStatus = "Хватит спамить";
                manageRepeatsAndAnswer([
                    "PKU_30.03_Хватит спамить_01.wav",
                    "PKU_30.03_Хватит спамить (повтор)_01.wav"
                ], true);

        # Не звоните мне больше
        state: NoMoreCalls
            q: $noMoreCalls
            q: $noMoreCalls || fromState = "/Start"
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Отказ от телемаркетинга";
                hangUp();

        state: DontKnow
            q: * (затрудня*/трудно) [ответ*] *
            # ой даже не знаю
            q: * жениться надо *
            q: * (боюсь/боюся) *
            q: * ($dontKnow/хз) * [что [мне] [можно] (делать/сказать/ответить)/{хорошо или плохо}/пока] * [$disagree/не $activate] * [пока] *
            q: * [$disagree] * (думаю/думаем/думать/подум*/обдум*/поразмышл*/*совет*/поссовет*/переговорим/переговорю/*размысл*/размышл*/изучить/изучу) *
            q: * [$disagree] * [надо] * (*дум*/*совет*/*размысл*/*размышл*/переговорим/переговорю/поговор* с $AnyWord) *
            q: * ((не могу/сложно) [сейчас/пока/сразу] * (дать ответ/ответить/реш*/понять)) *
            q: * {(могу/можно/нужн*/я/мы) * (подумать/подумать/подумаю/подумаем/обдумать/обдумаем/поразмышл*)} *
            q: * (подумаю/подумаем/подум*/(не|ну) знаю) $weight<1.3> * [$later/следующ* раз] *
            q: * [спасибо] * (подумаю/подумаем/подум*/(не|ну) знаю) * [подумаю/подумаем/подум*/(не|ну) знаю/спасибо/$unlimitedAgree или нет] $weight<1.4> *
            q: * (подумаю/подумаем/подум*/(не|ну) знаю) $weight<1.3> * || fromState = /Objections/Agreed/IsAgreed
            q: * {[я] * (теряюсь/потерялся/потерялась) * [{(опять/снова) * *ключ*} $weight<+0.6>]}  *
            q: * {(не (*могу/*можем)) * (сказа*/ответ*) [ничего]} $weight<+0.2> *
            q: * погоди* * || fromState = /Objections/Agreed
            q: * (в расплох/врасплох) *
            script:
                $temp.definedEndStatus = "Надо подумать";
                manageRepeatsAndAnswer([
                    "08.04.21_Не знаю, надо подумать.wav",
                    "PKU_30.03_Не знаю, надо подумать (повтор)_01.wav"
                ], true);

        state: DontNeedLimit
            q: * {(нет нужды) * лимит*} *
            q: * {(не (надо/нужн*/нужен/хоч*)/зачем) * лимит*} *
            q: * {[не] (удобно/неудобно) * [в] долг* [не]} *
            q: * {удобно * $pay * [[в] долг*] (по факту)} * $weight<+0.3>
            script:
                $temp.definedEndStatus = "Не нужен лимит";
                manageRepeatsAndAnswer([
                    "PKU_30.03_Мне не нужен никакой лимит_01.wav",
                    "PKU_30.03_Мне_не_нужен_никакой_лимит_повтор_01.wav"
                ], true);

        state: LeaveMgf
            q: $wantToLeaveMgf [$wantToLeaveMgf] 
            q: {[$noCallInterest $weight<-0.5>] $wantToLeaveMgf [$wantToLeaveMgf]} || fromState = "/Start"
            q: * {$freeOnlyNow * $wantToLeaveMgf} * $weight<1.3>
            audio: 03.02.21_уйти из мгф оля.wav
            script:
                $temp.definedEndStatus = "Переход к другому оператору";
                hangUp();

        # Чувствует подвох
        state: WhatsYourProfit
            q: * [[это/эта] * $service] * [$disagree/[$disagree] без] * (~выгода|подвох*|~обман|~обмануть|~обманывать|~скрытый|умолч*|умалчив*|{вам * зачем}|что вам|вам что|в чем * прикол) * [надо/нужно] * [активиров*/подключ*] * $weight<+0.03>
            q: * без (подвох*/обман*/развод*/~вранье) * $free * $weight<+0.1>
            q: * {~какой * (подвох*/обман*/развод*/~вранье) ((должен/должн*) быть) * [жду/ждешь/ожидаю/ожидаешь]} * $weight<+0.1>
            q: * (в дураках/делаете из меня дурака) *
            q: * скепти* *
            q: * не (верю/верим/верится/вранье) *
            q: * (~манипуляция/~манипулировать/впарива*) *
            q: * [$Number/р/руб*] * [что то] * (не/плохо/слабо) (верится/верю) * [$Number/р/руб*] *
            q: * {(вам/[для] вас/ваша) * [заключается] * ([[в] чем/какая] выгод*/выгодн*/не выгодн*/невыгодн*) [если * (звоните/названива*)/{(не стали) * (звонить/названива*)}]} * $weight<+0.1>
            q: * (настойчив*/заманчив*) $weight<+0.15> *
            q: * [точно] {[нет/нету/есть] [$2AnyWords] подводн* камн*} *
            q: * развод $weight<+0.2> *
            q: * (как то/слишком) дешев* *
            q: * (не/нет [никакого [к вам]/к вам]) (доверяю/доверия/доверяем) [ничему/вам] *
            q: * [с чем связ*/с чего/откуда] [такая] (щедрость/роскошь/розка) $weight<+0.2> *
            q: * {(ваша/ваш/мегафон*/оператор*/не бывает) * (~выгода/профит/во благо/просто [вот] так)} *
            q: * {почему * дешево} *
            q: * судя * как * уговаривае* *
            q: * зуб* * не заговаривай* *
            q: * подозрительн* * [$free] *
            q: * [что то] * (страшно/страшновато/боязно/странно/(не [$quite] так)/недоброе) *
            q: * [я] (боюсь/боимся) *
            q: * как* * цель* *
            q: * (точно/это) * (обман/вранье/ложь/враки) *
            q: * в чем подвох * $weight<+0.3>
            q: * {(вам/для вас) * что * это* * [*знач*]} *
            q: * {(не верю) * обман*} $weight<+0.2> *
            q: * [это] (развод*/развест*) $weight<+0.3>  *
            q: * {(другая/обратная) сторон* * [есть/присутствует]}  $weight<+0.3> *
            q: * {(подложи*/подклад*) * (поросен*/свин*)} $weight<+0.3> *
            q: * {сейчас * (столько/[очень] много/развелось) * (мошенников/обманщиков/проходимцев)} * $weight<+0.4>
            q: * {[понима*/понятно] * какой вам $2AnyWords смысл*} $weight<+0.3> *
            q: * чем * [я/мы] * [так] * (заслужил*/отличился/отличилась/отличились) *
            q: * ~какой * заслуг* *
            q: * {(за что) * (мне/нам) * [$good] * $service} *
            q: * {(сомнит*/подозр*) [весьма/достаточно/очень/слишком]} *
            q: * {хитр* (какие/какая) * [все]} *
            q: * {(вам/тебе/мегафон*) * (зачем/для чего) * (~бонус/~опция)} *
            q: * {(вам/тебе/мегафон*) * (зачем/для чего) это} *
            q: * {(вам/тебе/мегафон*) * (плюс*/достоинств*/преимущ*/бонус*/+)} *
            q: *  [$why] * {(мегафорн*/вы/ты) * (хитрит*/~хитрый/~хитрость/затева*/затея*)} *
            q: * ({лапш* ([на] уши) * веша*}/{сказк* * рассказы*}) *
            q: * {(~вы/~ты) * (зачем/для чего/почему)} * {$good * ($service/пункт*)} *
            q: * {$good * ($service/пункт*)} * {(~вы/~ты) * (зачем/для чего/почему)} *
            q: * {(почему/зачем/чего) * ($service/пункт*)} * ([с] того [ни] [с] сего/вдруг/внезапно) *
            q: * {$why * ([с] того [ни] [с] сего/вдруг/внезапно)} * ($service/пункт*) *
            q: * {$why * $good * $me} *
            q: * {(в связи с чем|$why) * (~такой/этот) ($service/пункт*)} *
            q: * {(что [за]/за что) [вдруг/это/$3AnyWords] (признат*/~благодарность/~почесть)} *
            q: * (чего/почему/отчего/причин*/за что) * (признат*/благодар*) *
            q: * {что * (мегафон*/$you) * (~иметь/~получать/~получить)} *
            q: * {[хочешь/хотите/пытаешься/пытаетесь] * [опять/снова] [(что/~какой) то] (впарива*/всучи*/впари*/втюхива*)} *
            script:
                $temp.definedEndStatus = "В чем ваша выгода";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "PKU_30.03_В чем ваша выгода_01.wav"
                ]);

        #А мне это выгодно?
        state: MyProfit
            q: * оно ((мне/нам) [вообще]/вообще) надо *
            q: * что * мне будет* *
            q: * {что [это/она] [мне/нам] [не] [$AnyWord] (дает/даст)} * $weight<+0.1>
            q: * [$wait/как ($pay/ложил*/клал*/пополнял*) так и буду] * {(мне/не/для меня) * (выгодно/$why/к чему) * [она/$service/это/ее]} * [понима*/понятно/ясно] *
            q: * [понима*/понятно/ясно/объясните] * [какой/какая] {(~мой/мне/для меня) * (~выгода/профит)} * [понима*/понятно/ясно/объясните] *
            q: * (точно/действительно) * выгодн* * [понима*/понятно/ясно] *
            q: * выгодн* * [точно/действительно] * [понима*/понятно/ясно] *
            q: * не выгодн* *
            q: * [то есть/не понял*/подожди*] * ($why/чего) * [мне/нам] * (подключ*/надо/нужно) * [предостав*/подключ*] *
            q: * [то есть/не понял*/подожди*] * {($why/чего) * (подключ*/надо/нужно) * [предостав*/подключ*]} *
            q: * {(чем/какое) * (отличается/отличие) * (моих/моего)} *
            q: * зачем * [{(брать/мне) * долг*}] *
            q: * {[понима*/*понятно] * (не (вижу/видим)/какой/в чем) * (смысл*/~суть)} $weight<+0.2> *
            # tts что мне тогда = что мне это даст
            q: * что мне тогда *
            q: * {хуже [то] ([не] (будет/станет)/не должно $2AnyWords (быть/стать))} *
            q: * {зачем [$AnyWord] (не надо)} *
            q: * {что * (буду иметь) ([от/c] этого)} *
            q: * {[~какой/в чём] [еще]} * {$advantages *  $service} * [$2AnyWords] *
            q: * {[~какой/в чём] [еще]} {($advantages/бонус*) * (~акция)} * [$2AnyWords] *
            q: * (не/нет) [$AnyWord] [$AnyWord] (понима*/*понятно) * (для чего/зачем) *
            q: * {$once * $enable} * {(что [$2AnyWords] (итог*/результат*)/$good)} *
            script:
                $temp.definedEndStatus = "В чем моя выгода";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "08.04.21_В чем моя выгода.wav"
                ]);

        # Что будет с тарифом, нравится текущий тариф / Что изменится?
        state: WhatAboutMyTariff
            q: * [то есть/не понял*/подожди*] * $tariff *
            q: * [то есть/не понял*/подожди*/с ~этот $service] * ((~платеж*/$pay) * (~прежний/по-старому/как [и] (до этого/раньше/сейчас))) *
            q: * [то есть/не понял*/подожди*] * {((тоже/(тот/та*/тем) [же]) [самый]) ($tariff/платеж*/*плата) * [оста*]} *
            q: * {[мой] * ($tariff/ничего) [не (измен*/помен*)] * [спасибо/хорошо/$tariff]} *
            q: * {([мой] $tariff/какие/пакет) * (измен*/помен*/менять*)} *
            q: * (доплачива*/доплати*/добавка/добавление/изымается/изыматься/вычита*) *
            q: * меня все окей *
            q: * {[$unlimitedAgree] * ([мой/наш/текущий/у меня] тариф) * (устраивает/устроит/нравится/не (*меняется/изменится))} *
            q: * {$tariff * (нравит*/устраивает/устроит/доволен/довольна/норм/нормальный)} *
            q: * {(~пакет/~трафик) * (устраивает/устроит/~хороший/достаточно/хватает)} *
            q: * [то есть/не понял*/подожди*] * (что * (изменит*/поменяет*)/(какая/чем/какой) * (разниц*/толк*)) *
            q: * [то есть/не понял*/подожди*] * что будет* *
            q: * [то есть/не понял*/подожди*] * {это * [не] влия*} *
            q: * {минуты * (добав*|предоставл*) * [как* услов*]} *
            q: * какого числа *
            q: * что * (изменится/*меняется/менять*) * опциях *
            q: * {то же самое * ($pay/пополн*) * *позже} *
            q: * {останется * (было/раньше/(то же/тоже) [самое]) * (или нет/только/кроме)} *
            q: * {платить * столько * [же/раньше]} *
            q: * {[*могу] * звонить * только * мегафон*} * $weight<+0.1>
            q: * ничего не изменится * $weight<+0.1>
            q: * $tariff переводить * $weight<+0.1>
            q: * {подключ* * буду * (говорить/разговаривать/пользоваться/звонить) * бесплатно} * $weight<+0.1>
            q: * ($internet/$tariff) * будет отличаться *
            q: * {$tariff * (будет/станет/~пойти) [меня/$later] * *минутн*} *
            q: * {$tariff * все включено} *
            q: * {$tariff * (останется/остается/нет/*меняется) (или/не) * (поменяться/*меняется/изменится)} $weight<+0.1> *
            q: * {($tariff/все/цен*/стоимост*)  * (останется/остается) [так* же/прежн*/(~который/как) * есть]}  *
            q: * {($pay/все) [[в (месяц/день)] $Number [руб*]] * (буду/было/будет/останется) * ((так/таким) (же/и)/также/как (сейчас/есть/$pay)/[по] прежн*)} *
            q: * {(на что) * повлияет} *
            q: * (получается/выходит/то есть/значит*) * {$phoneBalance * $rechargeBalance * $noReason} *
            q: * (если * не (по-моему/по моему/как $me)) * ((друг*/смен*/помен*/верн*) $tariff)*
            script:
                $temp.definedEndStatus = "Что изменится, изменится ли тариф";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    // "PKU_30.03_Что изменится_01.wav",
                    "Что_изменится_Изменится_ли_тариф_UPD_01.06.wav",
                    "PKU_30.03_Что изменится (повтор)_01.wav"
                ]);

        # Оставьте все как есть
        state: DontChangeAnything
            q: * [$disagree] * [$service] * {(все/мне/меня) * [все/всего] (устраивает/хватает) * [$unlimitedThanks/(что/как) есть]} * $weight<+0.13>
            q: * {(пусть/пускай/хочу/лучше) [[все] [будет/оста*]] * ((как/то что/по) * (есть/было/раньше/сейчас/старо*/прежн*))} *
            q: * ничего не хо* *
            q: * [$repeat<$disagree>] * [меня/я] (устраивает/привык*/справляюсь) * (как/что/так/$tariff) * (есть/сейчас) * $weight<+0.3>
            q: * [меня/я] {(устраивает/привык*/справляюсь/нравится) * (как/что/так/$tariff) * [есть/остаюсь/остав*]} *
            q: * (меня/мне) все (устраивает/нравится) *
            q: * {(устраивает/устроит/нравится) * (то что [у меня] есть)} *
            q: * мне хватает (того что/моего) [$tariff] $weight<+0.2> *
            q: * моторист *
            q: * {[пусть/пускай] (останется/остается/оставить/будет*/устраивает/$like) * (как/так) (есть/бы*/сейчас/остается/она/он)} *
            q: * [против $weight<+0.3>] [$disagree] * {(остав*/и все/пусть/пускай) * (как * (есть/было/сейчас))} * [$bye] * $weight<+0.1>
            q: * {остав* * (так * (есть/было))} *
            q: * {(не (хочу/буду/хотим/будем/надо/нужн*/делайте/хочет)) * [ничего] * (*менять/обновлять/обновлени*)} * $weight<+0.1>
            q: * {ничего * (не (*меняй* $weight<+0.5>/трогай*/подключай*/активируй*/актер/добавляй*/меняете/трогать/трогай*))}  *
            q: * {ничего * не подключай* * как (есть/было/сейчас/нахожусь)} *
            q: *  {[мой/мне/текущий/нынешний] тариф * (устраивает/хороший/не * (*меняй*/трогай*))} *
            q: * {[меня/нас] (все/и так) устраива*} *
            q: * (нам/мне/все/всего) * (хватает/*достаточно/норм*) *
            q: * {$tariff * [$change] [пока] * (не (собираюсь/собираемся))} * [{(не ($need)) * ничего}] *
            q: * {[так/как] сейчас * $good * ничего (не (надо/нужно/меня*/добавл*))} *
            q: * {(хочу чтобы/пусть/лучше/хотелось) * [оста*] * ((как/что) (было/есть/сейчас/обычно)/так и оста*)} * $weight<+0.2>
            q: * {[и так] ($good/неплохо) * без * ($service/нее/него/это*)} *
            q: * привык* * [сейчас] * [у меня] *
            q: * {я [пока] [$AnyWord] (доволен/довольна)} *
            q: * {пока * ничего * (не (нужно/требу*/*надо*))} *
            q: * [$disagree/опасно] * {(в прошлый раз/уже/*звонили) * поменяли [$tariff]} * $weight<+0.1>
            q: * [$repeat<$disagree>] * как* * есть [[у] меня] [связь/$tariff] * та* [лучше/пускай/пусть] [$2AnyWords] [и] (будет/останется/остается/не (~менять/~трогать)) * [$repeat<$disagree>/$tariff] * $weight<+0.1>
            q: * [не (надо/нужно)/$no] * [так/это] пойдёт *
            q: * [не (надо/нужно)/$no] * [так/это] пойдёт * || fromState = /Objections/Agreed/IsAgreed onlyThisState=true
            q: * не (трог*/меня*/обновл*/измен*/добавл*) * [у меня] * (ничего/$tariff/настройк*) * [ничего * не надо] * оставь* * (что/как) [$AnyWord] (есть/сейчас) *
            q: * [$disagree] * [не предлага*] * {(остав*/и все/пусть/пускай) * (как * (есть/было/сейчас))} * $weight<+0.1>
            q: * {(пускай/пусть) * (останутся/останется/будет/будут) как бы} *
            q: * {(пусть/пускай/лучше) * [ничего] * (не (*меня*/трога*/актер/$enable))} *
            q: * {[лучше] * [ничего]} * {(*меня*/трога*/актер/$enable) * (не (буде*/нужно/надо))} *
            q: * {(хочу чтобы/пусть/лучше/хотелось/пускай) * (оста*/будет/будут)} * {не (надо/нужно) * (представ*/$enable)} *
            q: * {(не (надо/нужно)) * (представ*/$enable)} * {(хочу чтобы/пусть/лучше/хотелось/пускай) * (оста*/будет/будут)} *
            q: * лучше [$AnyWord] [$AnyWord] {оставь* [$AnyWord] [$AnyWord] так} *
            q: * {(сохран*/оставь*) * ((тем/тот/такой) же/один/одним)} * {(момент/~день) * $rechargeBalance} *
            q: * {(нравится/люблю/любим/~хотеть/предпочит*) * (((тем/тот/такой) же/одним/один) ~день)} * $rechargeBalance *
            q: * $rechargeBalance * {(нравится/люблю/любим/~хотеть/предпочит*) * (((тем/тот/такой) же/одним/один) ~день)} *
            q: * {[ничего] (не $needLocal) * ((нет/нету) [такой] (необходимости/нужды/потребности))} *
            q: * [[у] (меня/нас)] * {([ничего] (не $needLocal)/(нет/нету) [такой] (необходимости/нужды/потребности)) $change} *
            script:
                $temp.definedEndStatus = "Ничего не меняйте";
                manageRepeatsAndAnswer([
                    // "PKU_30.03_Ничего не меняйте.wav",
                    "Ничего не меняйте_UPD_01.06.wav",
                    "08.04.21_Ничего не меняйте (повтор).wav"
                ], true);

        # Это бесплатно?
        state: ItsFree
            q: * [хорошо] {(это/эта) [$service] $2AnyWords (~платный/плотно/платно/[за] доп* плат*/$free/что [то/нибудь] стоит*)} *
            q: * {([не/как* (то/нибудь)] будет*/потом/будут) * (сним*/спишут/выписыва*/вычиты*/высчиты*/списыв*/никаких $2AnyWords скрытых/[дополнит*] $pay)} *
            q: * {[то есть/но $weight<+0.2>/если] это [будет*] бесплатно} [я/мы] [так] [понял*/понимаю/понимаем] * $weight<+0.1>
            q: * [хорошо] [то есть/получается] {[потом/никак*/ничего] * $pay * (не прид*/[не] надо/придется/требуется/подразумевается/нет)} [да] *
            q: * {ничего * (платить/оплачивать) (не нужно) (точно/да)} *
            q: * [абсолютно/полностью] (платная услуга/*платн*) *
            q: * {(сним*/спишут/списыв*/сколько/~стоимость/доплачивать/плотно/платное/$free) * подключ*} *
            q: * ($free/стоит дене*/за деньги) * или * [$pay/стоит*/нет/как/без] *
            q: * [$pay/стоит*] * или * $free *
            q: * $pay * или * [нет/не] *
            q: * [то есть/не понял*/подожди*] * $free *
            q: * [то есть/не понял*/подожди*] * (не [надо] (плат*/оплач*)) *
            q: * [то есть/не понял*/подожди*] * {(точно/дополнит*) * (ничего * ~стоить/$free/$unlimited/не * ~платить)} *
            q: * {(это/эта) [$service] (~платный/плотно/платно)} *
            q: * {(надо/нужн*/можно) * *оплачивать [$service]} *
            q: * {(сколько * стоить/стоимость/*как* (сумм*/цен*)) * (активиров*/активаци*/добавлен*/подключен*)} *
            q: * {(замучил*/надоел*) * деньги (~класть/~ложить/~вложить)} *
            q: * дорого *
            q: * взамен * что * (должен/должна) * $weight<+0.3>
            q: * {(должн*/должен/нужно/надо) [будет/буду] * (платить/оплачивать) [за (это/$service)]} *
            q: * {(точно/очень) бесплатно * больше ничего * (платить/оплачивать/переплачивать)} *
            q: * {[никаки*/сверху] * (денег/деньги/~плата) * [больше] (не (будет/надо/берете/берутся)/сним*/спис*/спиш*/взима*)} *
            q: * {это * [только] бонус*} *
            q: * {с меня * ничего (не (*надо*/нужно))} $weight<+0.2> *
            q: * {это * (точно/совершенно/абсолютно/совсем) * $free} *
            q: * {([не] будет) * (без/какая [то/нибудь/либо]/никаких) * (оплат*/комисси*/списани*)} * $weight<+0.2>
            q: * [то есть/не понял*/подожди*] * совсем $free *
            q: * {(выходит/получается/то есть) * [ничего] $pay * (не (буду/будем/стану/станем)) [за это]} *
            q: * [я] * чем [то/нибудь] * обязан* *
            q: * {добав* * дополнит* * $pay} *
            q: * {$free * ~никакой (нет/нету)} *
            q: * {$free * $later} * {не $enable * (платн*/за (~деньги/плат*))} *
            q: * {($later/поздно/не вовремя/невовремя) * $pay * (коммиси*/пеня/процент*/$money)} *
            script:
                $temp.definedEndStatus = "Это бесплатно";
                $session.questionAsked = true;
            audio: 08.04.21_Это бесплатно.wav

        state: Child
            q: $stateChild
            q: * {$stateComplaint * {(($call/~позвонить) на уроке) [* поруга*]} * $bye} *
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Ребенок";
                hangUp();

        # Где лк?
        state: WhereIsMyAccount?
            q: * {(где/как) * (лк/[личн*] кабинет*/аккаунт*/личный)} *
            q: * ~личный ~кабинет *
            script:
                $temp.definedEndStatus = "Где лк";
                $session.questionAsked = true;
            audio: PKU_30.03_Где лк_01.wav

        # state: IsThisAFreeCall?
        #     q: * {(этот/ваш) * звонок * бесплатный} *
        #     q: * {(не спишут) * (за * звонок)} *
        #     script:
        #         $temp.definedEndStatus = "Звонок бесплатный";

        # Вы робот?
        state: YouAreRobot
            q: $YouAreRobot
            script:
                $temp.definedEndStatus = "Вы робот после предложения";
                $session.questionAsked = true;
            audio: PKU_30.03_Вы робот (после предложения)_01.wav

        # state: StrangeNumber
        #     q: * {(~странный/~подозрительный) ~номер [телефона]} *
        #     q: * {почему вы [звоните/позвонили] c (такого/этого) ~номер [телефона]} *
        #     q: * это не мегафон* [номер*/телефон] *
        #     q: * звонит * с [номеров] мегафон* *
        #     script:
        #         $temp.definedEndStatus = "Странный номер после предложения";

        # Какой у меня тариф?
        state: WhatTariffIHave
            q: * скажи* * (я (тра*/*расход*)/у меня) * ([$Number] (мин*/руб*)/пакет*) * [$nowLocal] *
            q: * {(какой/как) ([у] [меня]) * ($nowLocal/обычно/было)} *
            q: * $nowLocal что [у] (меня/нас) *
            q: * какие * меня * [платн*] $service *
            q: * {моем тарифе * (минут*/есть безлим*)} *
            q: * {(вопрос*/информаци*/инф*) * (мой/меня/моем) * [нынешн*/текущ*] $tariff} *
            q: * {(какой/на каком) ([у] [меня]/[я]) * $nowLocal [$tariff]} *
            q: * {($tariff/(сколько/как) [я/у меня] [$AnyWord] (плачу/уходит)/(должн*/должен) (платить/оплачивать)) * $nowLocal} * $weight<+0.1>
            q: * [не понял*] * [(сейчас/обычно)] * {(как*/сколько) * меня} * ($tariff/абонентск*) * [$nowLocal] *
            q: * [$nowLocal] * ($tariff/абонентск*) * {(как*/сколько) * меня} * [(сейчас/обычно/сегодня*/данный момент)] *
            q: * до этого какой * [тариф] *
            q: * [посмотр*/скажи*/подска*] * {[сколько/какая] * $nowLocal * ($pay/абонентск* *плата /*плата/выходит * (месяц/день))}  *
            q: * [посмотр*/скажи*/подска*] * {(сколько/какая) * $nowLocal * (абонентск* *плата /*плата)} $weight<+0.4>  *
            q: * (сказать/скажи*) * (сколько) * меня  *
            q: * [посмотр*] * {(стоимость/сумм*) * (сейчас/обычно)} *
            q: * {подписк* * (~убрать/~снять)} *
            q: * {(снимают/списывают/$pay/(сейчас/у меня) * [$tariff]) * $Number рубл* * (((каждый/[раз] в) (день/месяц)/ежедневн*/ежемесячн*)/$nowLocal/оста*)} *
            q: * {сколько * (минут*/$internet) * [сколько] * [(минут*/$internet)]} * [есть] * [бесплатн*] *
            q: * {(снимают/списывают/$pay) * $Number числа} *
            q: * {(заканч*/конч*) * (баланс*/$money) * ($Number [числа])} * $weight<+0.2>
            q: * {(что/сколько) * в (месяц/день) * выходит} *
            q: * {(что/какой) * $tariff * (у меня/мой)} $weight<+0.2> *
            q: * {(сколько/как много) * $money на $phone} *
            q: * {($nowLocal/[у] меня) * ([на] мегафон*) * $free} *
            q: * {уже * [есть/имеется]} * {$Number [бесплат*] минут*} *
            q: * {$Number [бесплат*] минут*} * {уже * [есть/имеется]} *
            q: * { {[$me] * *дешев* * (что (б/бы)/чтоб*)} * {[$me] [очень/сильно/прям*/слишком] (дорого [звонить]/{(много/куч*) [$chargeOff/уходит] $money})} } *
            q: * { {*дешев* * (что (б/бы)/чтоб*)} * {[$me] [очень/сильно/прям*/слишком] ((дорого/дороговато/затратно) [звонить]/{(много/куч*) [$chargeOff/уходит] $money})} } *
            script:
                $temp.definedEndStatus = "Какой у меня тариф";
                $session.questionAsked = true;
            audio: PKU_30.03_Сколько я сейчас плачу в день_01.wav

        # Отправьте предложение письменно
        state: SendMeMessage
            q: * {(пошли*/вышли*/пришли*/получить/отправь*/послать/выслать/сброс*/отправить/где/найти/есть/можно/пришлете) * [мне/нам] (письменно/письмом/на почту/в письменной форме/смс*/смске/смской/sms/[более] *подробн*/текст*/почита*/прочт*/прочитал*/сообщение*)} *
            q: * [можно] (~почитать/~прочитать/прочт*/читать/прочесть/{(где*/$lk) * посмотреть}) * [о/об/про] [~этот] ($service/информаци*/$tariff) *
            q: * (прочитать/прочт*/почитать) $weight<+0.2> *
            q: * [о/об] $service * (почтитать/прочитать/прочт*/читать/прочесть/где* * посмотреть) *
            q: * {(почему/с чем связан*/в связи с чем/по какой причине/объясн*) * ((рамка*/врем*) * (разговор*/звон*))} $weight<+0.1> *
            q: * {(*могу/*можем/можно) * (*смотреть/найти) * ($lk/~приложение/~сайт)} *
            script:
                $temp.definedEndStatus = "Где можно почитать об опции";
                $session.questionAsked = true;
            # audio: PKU_30.03_Где можно почитать об опции_01.wav
            audio: 16.06_PKU_Где можно почитать об опции.wav

        # Вы меня не понимаете
        state: YouDontUnderstandMe
            q: * [меня] * не (понимае*/поняли) [меня] *
            q: * (ты/вы) * не (понимае*/понял*) [меня] *
            script:
                $temp.definedEndStatus = "Вы меня не понимаете";
                manageRepeatsAndAnswer([
                    "PKU_30.03_Вы меня не понимаете_01.wav",
                    "PKU_30.03_Вы меня не понимаете (повтор)_01.wav"
                ], true);

        # state: HowDidYouGetMyNumber?
        #     q: * {(как/откуда/откудова/где) * [нашли/нашла/нашел/достал*/откопа*/этот/мой/наш/вас] * (номер*/телефон*)} *
        #     q: * {(кто/как*) * (дал*/передал*/сообщил*/отправил*) * (номер*/телефон*)} *
        #     q: * {(мой/свой) (номер*/телефон*)} *
        #     script:
        #         $temp.definedEndStatus = "Откуда у вас мой номер после предложения";

        # Нужно ли прийти в офис?/как подключить?
        state: HowToActivate?
            q: * что *делать *
            q: * [там] {как [это] (подключ*/*делать/делается)} *
            q: * не (знаю/понимаю/знаем/понимаем) что делать *
            q: * {(должен/должны/должна/надо/нужн*) * (приходить/прий*/прид*/зайти/зайд*)} *
            q: * добав* * или [мне] (надо/нужн*) *
            q: * {что * (я/мне)} * [должен/должна/нужн*] * (*делать/должен/должна) *
            q: * что * [от] (меня/мне) * требуется *
            q: * (когда/сейчас) [буд*/мне] добав* *
            q: * (что/как/чтобы/каким образом) [ее/его] * (активаци*/подключ*/активиров*/активаци*/добав*/включ*) *
            q: * {что [я/мне] [для этого] [должен/должна/должно] [$AnyWord] *делать} *
            q: * {что [вам] [от] (меня|мне) [$AnyWord] (требуется/надо/нужн*)} *
            q: * {что вам [от меня] [$AnyWord] (надо/нужн*)} *
            q: * {что вы [от меня] [$AnyWord] хотите} *
            q: * {что [для этого] [мне] (нужно/надо) [сделать/делать]} * $weight<+0.1>
            q: * {для этого [мне] (нужно/надо) [сделать/делать]} * $weight<+0.1>
            q: * {(что/чего/что-то/что то) * [меня/мне] (*требуется/нужн*/надо/должна) * [подключ*/оформ*]} *
            q: * {мне $2AnyWords (придется/придётся) * [подключ*/оформ*]} *
            q: * {(мне/меня) * (придется/придётся) * (подключ*/оформ*)} *
            q: * {(не знаю) как * (подключ*/активиров*/активаци*/добав*/оформ*/*делать)} *
            q: * {как* * требовани* * [подключ*/оформ*]} *
            q: * {что [для этого] [мне] (нужно/надо) [$AnyWord]  [$AnyWord] (сделать/делать/ответить/отвечать)} *
            q: * [то есть/$wait/$dontUnderstand] * {(подключ*/оформ*/активиров*/активаци*/добав*) * (нужно/идти/пойти/подой*/приходит*/подойд*/зайд*/зайт*) * ($office/мегафон/к вам/[в] $lk)} * [то есть/$wait/$dontUnderstand] *
            q: * [то есть/$wait/$dontUnderstand] * {(подключ*/оформ*/активир*/активаци*/добав*/включ*) * (соглаш*/соглас*/нужно/надо) * [это]} *
            q: * [то есть/$wait/$dontUnderstand] * (смогу/как/какое) [он*/ее/его/это/то] (подключ*/активир*/оформ*/добавить) * [то есть/$wait/$dontUnderstand] *
            q: * [то есть/$wait/$dontUnderstand] * {(нужно/надо) * (приходит*/идти/пойти/подой*) * ($office/мегафон/вам)} * [то есть/$wait/$dontUnderstand] *
            q: * [то есть/$wait/$dontUnderstand] * {(нужно/надо) * (приходит*/идти/пойти/подой*/прийти) * [$office/мегафон/вам]} * [то есть/$wait/$dontUnderstand] *
            q: * [то есть/$wait/$dontUnderstand] * {уже подключ* * согласи*} * [то есть/$wait/$dontUnderstand] *
            q: * [то есть/$wait/$dontUnderstand/в смысле] * {уже (подключили/подключен*/предоставили/активировали/включили/предоставлен*/активирован*/включен*) * [$service]} * [то есть/$wait/$dontUnderstand] *
            q: * [то есть/$wait/$dontUnderstand] * {(подключ*/оформ*/активир*/активаци*/добав*) * (нужно/могу) * (согласи*/подтвер*/сказать да/подтвержд*)} * [то есть/$wait/$dontUnderstand] *
            q: * {(нужно/надо/требуется/ждете/как*/достаточн*/довольно/хватит) * [мо*/меня/моего] * (согласи*/подтвер*/что/ничего/~действие/слова/соглаш*)} *
            q: * [$unlimitedThanks] * {(от меня) * (*чего/что) * (надо/нужно/требуется) * [больше/еще]} * $weight<+0.1>
            q: * {(сам/сама/само/соглас* $2AnyWords (сразу/тут же)) (активируется/подключится/включится)} *
            q: * (~сам) * ((будете/будешь) делать/активир*/подключ*/включ*/сдела*) *
            q: * {(спрашиваете/просите) * (моего/его/мое) (согласи*/одобрени*)} *
            q: * (обяза* * или (не/нет)*/{(можно/могу/може*) * отказ*}) *
            script:
                $temp.definedEndStatus = "Как активировать";
                $session.questionAsked = true;
            audio: PKU_30.03_Как активировать_01.wav

        # Что изменится?
        # state: WhatWillChange
        #     q: * [то есть/не понял*/подожди*] * (что * изменит*/(какая/чем/какой) * (разниц*/толк*)) *
        #     q: * [то есть/не понял*/подожди*] * что будет* *
        #     q: * [то есть/не понял*/подожди*] * {это * [не] влия*} *
        #     script:
        #         $temp.definedEndStatus = "Что изменится";

        # Всегда будет бесплатно?
        state: IsItAlwaysFree
            q: * {[это] (постоянно/временно/*всегда/в (дальнейшем/будущем)/сколько врем*/дальше/потом/совсем) * [тоже] [будет/останется/измен*] * ($free/финанс*)} * [(станет/будет) * $pay/или нет] *
            q: * {((на/до) како* * (срок/период)/надолго/долго) * (подключ*/добавля*/длиться/длительность/продолжительн*/постоянно/или) $free} *
            q: * {(какой/кого/какого) * (период [врем*]/времен*) $free} *
            q: * (какой (срок/период)/надолго/долго) * (подключ*/добавля*/длиться/длительность/продолжительн*) $free *
            q: * сколько * тред * $free
            q: * [на] (сколько [$AnyWord] (дней/месяцев/$service)|какое время) * $free
            q: * [это [будет*]/будет*] насовсем * $free
            q: * какой [то] срок * $free
            q: * {$service (как долго/[на] какой срок/[на] постоянн* основ*) * $free} *
            q: * (как долго/[на] какой срок/[на] постоянн* основ*) * (будет*/продлится/длиться/действовать) * $free
            q: * сколько * (продлится/длиться/действовать/тред) * $free
            q: * {(потом/(этот/текущий/следующ* месяц*)) [не] [$AnyWord] (придется|надо|станет/буд*) * (плат*/взимать*/взымать*)} *
            q: * {[$free *] потом ([не] (начнете|начну*)) * [$money] ($pay/$chargeOffNow)} *
            q: * (сегодня/сейчас) ($free/нет) * (завтра/потом) [$service] $2AnyWords плат* *
            q: * не (будет/станет) * (платн*/платить/не $free) * [через] *
            q: * сейчас $free * а (потом/*позже) *
            q: * {*совсем * $free или * месяц* } *
            q: * {плат* * [не [$AnyWord] (буду/будет/*надо*/нужно)] * никогда} *
            q: * {([не] * $pay/$free) * [(1/один) месяц*]} * [([или/а] (потом/*позже/дальш*) * (также/тоже/что/бесплат*/не $pay/сколько)) $weight<+0.2>] *
            q: * {$service * (~такой/$free) [$AnyWord] останет*} $weight<+0.3> *
            q: * сейчас $2AnyWords $free * через [$Number] (~день/недел*/месяц*) (будет*/станет*/начнет*/будут/станут/начнут) [$perDay/$perMonth] (сниматься/связаться/$pay/[по] $Number [[по] $Number] руб*) *

            script:
                $temp.definedEndStatus = "Всегда будет бесплатно";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "08.04.21_Опция всегда будет бесплатной, потом не начнут списывать за нее деньги.wav",
                    "08.04.21_Опция всегда будет бесплатной, потом не начнут списывать за нее деньги (повтор).wav"
                ]);

        # Зачем звоните, если бесплатно
        state: WhyCallIfFree
            #asr вывесить = в известность
            q: * {($why/[а] чего/смысл/[не] обязательно) * (звоните/этот звонок/звонить/набираете/набрали/спрашива*/задае* [этот] вопрос/участи*/ведом*/известность/вывесить/согласие/не подключ*) * [если [это] {[$service/скидк*] * ($free/платить * [не (надо/нужно)])}/планируе*/собирае*]} * $weight<+0.15>
            q: * {$why * (не (активир*/подключ*/делае*)) * [так/просто/сразу/сами]} $weight<+0.2> *
            q: * вы * сами * подключили *
            q: * {сдела* [бы] (сам*/без моего согласия)} *
            q: * {(могли бы просто добавить/можно * добавить/подключили бы) * $free}  *
            q: * { [не] добавили [бы] * (раз * бесплатно/бесплатная)} *
            q: * {[не] (понимаю зачем) [вы] [мне] звоните (если [это] * бесплатно/бесплатная)} *
            q: * {$why * (не (активир*/подключ*/делае*)) * (так/просто/сразу/сами)} $weight<+0.3> *
            script:
                $temp.definedEndStatus = "Зачем звоните если бесплатно";
                $session.questionAsked = true;
            audio: PKU_30.03_А_если_она_бесплатная,_зачем_вы_мне_звоните_01.wav

        # Потом начнете деньги снимать
        state: AfraidWontBeFree
            q: * {(начн*/будет*/придется) * (снимать/списывать/высчитывать/за/отдавать) * $money} * [обман*] *
            q: * {(стан*/стать/появится/сделаем/сделать) * $pay} $weight<+0.1> *
            q: * {(уже/один раз/как*) * [согласил*] * поплатил*} * $weight<+0.1>
            q: * {(уже/один раз/как*) * согласил* * [якобы/типа] заманчив* предложени*} *
            q: * [не знаю] * {(сомне*/сомнит*/подозр*/самом деле/(действительно/*правд*/настоящ*) [ли]) * ([не] $free/[не надо] * ($pay/тратить))} * $weight<+0.2>
            q: * {$free * (не бывает/ничего * нет)} * $weight<+0.2>
            q: * {ничего * $free (нет/не бывает)} *
            q: * {[боюсь/страшно/боязно] * (активир*/подключ*/сохран*/добав*/прибав*) * (деньг*/денег*/потом) * (снимете/снимите/спишете/спишите/возьмете/улетают/улетят/списывались/снимались/снимать*/{буд* (должен/должны)})} * $weight<+0.2>
            q: * $freeOnlyNow * $weight<1.3>
            q: * {((не/нет) * (вер*/довер*)) * $free} * $weight<+0.2>
            q: * {обеща* * (золотые/из золота) (горы/годы/город)} $weight<+0.2> *
            q: * [$disagree] * {(потом/в конце) * (выяснится * ((стоит денег)/не бесплатно)/(скажете/скажите/попросите/потребует*) * $pay/плохо)} * $weight<+0.2>
            q: * {(бесплатн* сыр * мышеловк*) * [я $2AnyWords [человек] $2AnyWords взрослый]} * $weight<+0.4>
            q: * {мышеловк* * [сыр]} $weight<+0.2> *
            q: * {(*платный сыр/заманух* [как* то])} *
            q: * {$disagree * кредит*} *
            q: * {(понимаю/понятно/ясно) что * (не $free)} *
            q: * говорите одно $2AnyWords делаете $2AnyWords друг* *
            q: * {[ваши/у вас] слова расходятся [с/и] действи*} *
            q: * финансов* положени* *
            q: * {(был*/подключ*/очередн*/включ*) * ([$free/столько/куч*/полно] * (платн*/плотн*/всякие) $service) * [боюсь/страшно/боязно]} $weight<+0.2> *
            q: * {(потом/позже/в (итоге/послед*)/(станет/окажется)) * ((вместо/не) $Number [руб*] [а] $Number [руб*]/[гораздо/намного] дороже/подорожает)} *
            q: * [*ключит*/предостав*] * {(потом/позже/в (итоге/послед*)) * ((окажется/будет/будем/буду/дерёт*/драть) * ($Number [руб*]/больше стоить/дороже/$pay))} * [не (надо/нужно)] * $weight<+0.2>
            q: * {(потом/в конце) * (снимать/списывать/высчитывать/за/нужн*/улетят/снимут) * $money} *
            q: * {[и так/уже] (на подключали/наподключали) $AnyWord (всяких/всякого)} *
            q: * (~оно) $free * (оказ*/на самом деле/потом/дальше) * (платн*/не $free/иначе/измен*/кругл* сумм*/снимать/высчитывать/*дорож*/дорог*) * $weight<+0.3>
            q: * [уговор*/уговар*] * {(*обещаете/*обещат*/*обещают) * (оказ*/окажется/на самом деле/потом/дальше/итог*) * [наоборот/по-другому/по другому]} *
            q: * $free * ($agree/$good/не плохо/неплохо) * [только/лишь/главное/если] [бы] * не [оказал*/выясн*/обнаруж*] [$2AnyWords] (обманите/обманете/обман/мошенн*/хитр*/~врать/врань*/врун*/врак*) * [придумаете/передумаете/был*] *
            q: * (потом/через/*после*) * (тридорог*/{(кучу/много/больше) $money}) * [берёт*/возьмёт*/дерёт*/$chargeBalance] *
            q: * {[берёт*/возьмёт*/дерёт*/$chargeBalance] * (потом/через/*после*)} * (тридорог*/{(кучу/много/больше) $money}) *
            q: * [берёт*/возьмёт*/дерёт*/$chargeBalance] * (тридорог*/{(кучу/много/больше) $money}) * (потом/через/*после*) *
            q: * {заранее * [$money] ~снимать} *
            q: {(вы/ты/$botName/мегафон*/микрофон*) ([у] [меня/нас])} {[$money] (~хотеть/будете/будешь/станете/станешь/начнете/начшешь) (спис*/сним*/снять/забир*)}
            q: {[$money] (~хотеть/будете/будешь/станете/станешь/начнете/начшешь) (спис*/сним*/снять/забир*)} {(вы/ты/$botName/мегафон*/микрофон*) ([у] [меня/нас])}
            q: * (не [$quite] [$you] доверя*) * $dontKnow *
            q: * {{$chargeOff $2AnyWords $money} * {$leave * $mobileOperator}} *
            q: * {[потом/после/когда] * (начн*/начин*/буде*) накручивать} * $weight<+0.2>
            q: * {(все равно) * [$money] (снимут*/снимет*/спишет*/спишут*/{(будут/будет*) $chargeOff})}*
            script:
                $temp.definedEndStatus = "Потом начнете деньги снимать";
                manageRepeatsAndAnswer([
                    // "PKU_30.03_Знаю я вас_01.wav",
                    "Знаю_я_вас,_начнёте_деньги_сейчас_снимать_UPD_01.06.wav",
                    "08.04.21_Знаю я вас, начнёте деньги сейчас снимать (повтор).wav"
                ], true);

        # Плохо ловит
        state: BadReception
            q: $badReceptionState
            script:
                $temp.definedEndStatus = "Плохо ловит";
                manageRepeatsAndAnswer([
                    "PKU_30.03_Сеть плохо ловит_01.wav",
                    // "PKU_30.03_Сеть плохо ловит (повтор)_01.wav"
                    "16.06_PKU_Сеть плохо ловит (повтор)_01.wav"
                ], true);

        # Уже пользовался
        state: AlreadyInUse
            q: * {[когда*] (уже/был*/и так) * (*пользо*/подключал*/работ*/испытывал*/пробовал*) * [[~такой/~этот] (бонус*/пакет*/$tariff)]}  *
            q: * {(уже/был*/и так) * (*пользо*/работ*/испытывал*/пробовал*) * [~это $weight<+0.5>]}  *
            q: * {[такая] была * подключена ($service/штук*)} * $weight<+0.2>
            q: * {такая была * [уже] ($service/штук*)} $weight<+0.2> *
            q: * [$unlimitedThanks] * {(уже/был*) * (*пользо*/подключ*/работ*/отказывал*/подобн*/~такой)} * [$service/бонус*] * [$unlimitedThanks] *
            q: * [$unlimitedThanks] * {(подключали) * $service} * [$unlimitedThanks] * $weight<+0.2>
            q: * {(уже/[у меня] был*) * (~такой/стоит) * ($service/~штука/опыт*/бонус*)} * [[ничего] не (было/увидел/давали/предоставляли) $money] * {[$disagree] * [пока не]} * $weight<1.3>
            q: * {(уже/был*) * (так*/стоит/подключ*) * ($service/такое)} * ((если/пока) не (пополн*/полож*) $2AnyWords не работ*/{(надо [было]/приходилось) ($pay/класть/пополнять)}) * $weight<+0.1>
            q: * {[уже/был*] * (*польз*/подключ*/есть/работ*/включ*) * $service} *
            q: * [$disagree] * {(у (меня/нас)) [уже|же] было (он/она/это/такая/такое/такой) [$service]} * [$disagree] *
            q: * [$disagree] * {[уже/раньше] * (было/была/*пользовал*/пробовал*/подключ*) * (отключил*/отключал*/неудобн*/не (удобн*/понрав*)/аналогич*) * [сам* $weight<+0.1>]} * $weight<+0.15>
            q: * {[$disagree] * уже * отключ*} *
            q: * [$disagree] * {[уже] * (отключал*/отключил*/открутил*/отказывал*/отказал*) * [не (понрав*/нужн*/нужен)/предоставлял*/в (прошлый/предыдущ*) раз]} * $weight<+0.1>
            q: * {[$disagree/был* подключен*] * (она/он/[такая/такую/такой/такое/похожая/похожую/похожий/похожее/подобный/подобную/подобная/подобное] $service/что то (такое/подобное/похожее)) * [был*] * (раньше/уже/было/была/помню)} * [отключ*/выключ*/включ*/$disagree $2AnyWords (снова/опять/обратно/возвращать)] * $weight<+0.1>
            q: * мне * не понравил* * [$disagree] * $weight<+0.1>
            q: * {{уже был*} * (не (устраивает/понравил*))} $weight<+0.3> *
            q: * {[$disagree] * [уже] * ~сталкиваться} *
            q: * {(меня/мне) * $once  ($enable/*делал*)} *
            q: * {(уже/раньше/до этого/назад/прошл*) * (было/была/*пользовал*/пробовал*/подключ*)} * [сколько] * [мож*/мог*]
            q: * уже * {(сколько можно) * $activate} *
            script:
                $temp.definedEndStatus = "Уже пользовался";
                manageRepeatsAndAnswer([
                    "Уже пользовался_UPD_01.06.wav",
                    "Уже пользовался (Повтор)_UPD_01.06.wav"
                ], true);

        # Уже пользуется
        state: InUseNow
            q: * {{[мне/я] [же/ж] (уже/же/ж/даже/и так/итак/сейчас/[у] меня/[у] нас/ня)} * (*пользу*/подключен*/подключил*/работает/пробу*/стоит/установ*/активиров*/активн*) * ([так*/~это] [пакет*/$tariff/~она/~он/$service/штук*])} *
            q: * {{[же/ж] [уже/же/ж/даже/и так/итак/сейчас/[у] меня/[у] нас/ня]} * (*пользу*/работает/пробу*/стоит/установ*/активиров*/активн*) * ([так*/~это] (пакет*/$tariff/~она/~он/$service/штук*))} *
            q: * {сейчас * ($service/$tariff) * (разговарива*/обща*/звон*/хож*)} *
            q: * {(у (меня/нас)/мне/ня) * {[же/ж] [уже/даже] есть} * ([так*/~это] * (пакет*/$tariff/~она/~он/$service/штук*/так*/у ~вы))} *
            q: * {(у (меня/нас)/мне/ня) * {[уже/даже] [же/ж] есть} * ([так*/~это] [там] [пока] (пакет*/$tariff/~она/~он/$service/штук*/так*/у ~вы))} *
            q: * {(у (меня/нас)/я/мне/ня) * {[же/ж] (уже/даже) есть} * ([так*/~это] * (пакет*/$tariff/~она/~он/$service/штук*/так*/у ~вы))} *
            q: * (меня/мне/есть) [* так] ($service/$tariff/пакет*/у ~вы) *
            q: * {(у меня/нас/мне) (уже/и так/итак/сейчас) * ([это*/так*] ($tariff/$service/штук*/пакет*))} *
            q: * {(это * ~мой) * [нынешний/сейчас] ($tariff/$service/пакет*)} *
            q: * {(уже/даже/и так/итак/сейчас) (*пользу*/подключен*/подключил*/работает/пробу*/стоит/установ*/активиров*/активн*) [пакет*/$tariff/~она/~он/$service/штук*] ([у] (меня/мне/нас)/я/ня)} *
            script:
                $temp.definedEndStatus = "Уже пользуется";
                manageRepeatsAndAnswer([
                    "Уже пользуется.wav",
                    "Уже пользуется (повтор).wav"
                ], true);

        # Автоплатеж
        state: AutoPay
            q: * (автоплатеж*|платеж автоматом) * $weight<+0.3>
            q: * {(автоматическ*|автоматом|[$AnyWord] карт*) * ($pay|спис*|пополн*|баланс*|отправ* $money)} * $weight<+0.2>
            q: * {(запрограммиров*|[в] (программ*|$lk|мобильн* банк*)|само) * (*плат*|[спис*] [с] (~карта|карточк*)|спис*|снима*|каждый месяц)} * $weight<+0.15>
            q: * {платеж * сам * *банк*} * $weight<+0.3>
            script:
                $temp.definedEndStatus = "Автоплатеж";
                manageRepeatsAndAnswer([
                    "PKU_30.03_У меня автоплатеж_01.wav"
                ], true);

        state: AlwaysPayInTime
            # есть право = исправно
            q: * {[$disagree/спасибо/поскольку/так как/и так] * $inTime * ([привык*] $pay/получу/плачу/кладу $weight<+0.2>/ложу/вношу/ношу/~положить $weight<+0.2>/выполн*/перевожу) * [всегда/{(не/нет) [$AnyWord/нужн*/надо] * (проблем*/необходим*/$service)}/$disagree]} * $weight<+0.02>
            q: * [$disagree] * {просроч* * не* [бывает/случается] * никогда} * [$disagree] *
            q: * [$disagree] {(контролир*/пополняю/пополняем/слежу/следим/наблюдаю/наблюдаем/[всегда] (плюс*/положительн*/полн*) [всегда]) [за] [~свой] (баланс*/расход*/$money)} * [не люблю * (долг*/*долж*)] * $weight<+0.3>
            q: * {(минус*/нол*/нул*/на любого/не [мог*/смог*] [$AnyWord] ($pay)) * не (приходится [*ходить]/быва*/случается/ухожу/уходить/дохожу/доходить/довожу/доводить/остаюсь/допускаю)} * $weight<+0.2>
            q: * {(минус*/нол*/нул*/на любого/не [мог*/смог*] [$AnyWord] (баланс*)) * (не (приходится [*ходить]/быва*/случается/ухожу/уходить/дохожу/доходить/довожу/доводить/остаюсь/допускаю))} * $weight<+0.35>
            q: * {[так/я/меня] * ([не $weight<-0.2>] удобно/всегда/запас*) * (класть/*ложить/лежат) (деньги/денежки)} * $weight<+0.1>
            q: * [отключал*/отключил*/открутил*/отказывал*/отказал*] *{(пополняю/плачу/привы* * (*платить/оплачивать/класть/*ложить)/кладу/ложу) * (вовремя/время подходит/законопослушн*/определен* (дни/дням/день/время/числ*)/кажд* месяц/$Number числа/(как/когда/как только) * {((заканч*/конч*) $money/оповещ*)})} *
            q: * есть дата $pay *
            q: * {((удобн*/как) $pay * (как/так и) [$AnyWord] [$AnyWord] (сейчас/$pay))} * $weight<+0.2>
            q: * {((удобн*/как) [$pay] * (как/так и) [$AnyWord] [$AnyWord] $pay)} *
            q: * я * [и так/сейчас/в любом случае] * (всегда/все да/все время/и так/нол*/нул*) * на связи * [{баланс* $2AnyWords слежу}] *
            q: * {не (бываю/бывает/случается/приходи*) * (без/$disagree/не было) связи} *
            q: я [$pay] постоянно
            # на лето = нуля то
            q: * {[никогда] [у меня] * (не бывает/нет) * ([по] нул*/[по] нол*/на лето)} $weight<+0.2> *
            q: * {(кладу/класть/полож*/лож*/*ложу) * [деньг*/[на] (счет*/баланс*/телефон*)/насчет] * (за несколько/в любое время/сразу/всегда/когда [мне/нам] [это] (надо/нужно/требуется)/ровн*)} $weight<+0.15> *
            q: * {($phone/мегафон*/счет*) * (всегда/постоянно/кладу/обычно) (кладу/в срок/есть/лежат/находятся/хватает/в избытке/достаточно) * (денег/деньги/средств*/денеж*)} *
            q: * {(достаточно/хватает/в избытке/любое время/загодя/привыч*) * (оплач*/оплат*/плачу/плат*) * [обычно/всегда/постоянно/$lk]} *
            q: * {(форс мажор*) * (не (бывает/происходит))} *
            q: * {(всегда/обычно) * (есть/присутствуют/на связи) * ($money/средств*)} $weight<+0.3> *
            q: * {(всегда/обычно) * (на связи)} *
            q: * {(не бывает) * (такой/таких/подобной/подобных/непредвид*) (ситуаци*/случа*)} *
            q: * {(никогда/всегда/ничего/ни разу) * (не (просрач*/просроч*))} *
            q: * {сразу * *плач*} $weight<+0.2> *
            q: * [$no] * [$thanks] * {(в люб* момент*/когда угодно/всегда/все равно) * (*могу/*можем/успеваю/буду) [$inTime] (положить/пополнить/пополнять/закинуть/*платить/вносить) * [$money/[в] $lk]} *
            q: * {(нол*/нул*/$minus/отрицательн* баланс*) $2AnyWords ([у] меня) [никогда/когда (нибудь/то) будет] (не будет/нет/нету/не думаю)} * $weight<+0.1>
            q: * {(нол*/нул*/$minus/отрицательн* баланс*) $2AnyWords ((ни разу/не раз) [[у] меня] ([такого] не было)/не может $2AnyWords быть)}
            q: * {[всегда/и так] (слежу/(привык*/привыч*) следить) [за/чтобы] * ((баланс*/расход*/$money) [был* положит*/не (заканч*/кончал*)])} *
            q: * {[я] [$pay]} * {(остаются/есть) * деньги} $weight<+0.2> *
            q: * {не бывает * (денег*/денеж*) (не было)} $weight<+0.2> *
            q: * [я/мы] * [вроде] * $pay и $pay *
            q: * {(~этот ~месяц/$monthTitle) * (*платил*/*плачивал*/положил*/пополнил*/*плачен*/закинул*)} *
            q: * {(есть/имею) возможность * ($pay/~класть/~ложить/полож*/пополн*/закин*/закид*) * (когда (угодно/нужно/*треб*/*конч*/*канч*))} *
            q: * [$no] * [$disagree] * [говорю] * {$rechargeBalance * $Number [числ*]} * {$chargeBalance * $Number [числ*]} * [(не/$no) [$AnyWord] (позж*/позд*)] *
            q: * [$no] * [$disagree] * {$chargeBalance * $Number [числ*]} * {$rechargeBalance * $Number [числ*]} * [(не/$no) [$AnyWord] (позж*/позд*)] *
            q: * никогда * не  [$AnyWord] [$AnyWord] [$AnyWord] {(задерж*/просроч*/пропус*/забыв*/*забыт*) * (плат*/оплат*/оплач*/заплат*)} *
            q: * не [$AnyWord] [$AnyWord] [$AnyWord] (задерж*/просроч*/пропус*/забыв*/*забыт*) * {никогда * (плат*/оплат*/оплач*/заплат*)} *
            q: * {никогда * (плат*/оплат*/оплач*/заплат*)} * не [$AnyWord] [$AnyWord] [$AnyWord] (задерж*/просроч*/пропус*/забыв*/*забыт*) *
            q: * {(положено/нужно/необходимо) * $rechargeBalance [значит] [$AnyWord] [$AnyWord] $rechargeBalance} *
            q: * {никогда не * (~ноль/~нуль/нуле*/ноле*/0)} *
            q: * давно ~сидеть {никогда не * (~ноль/~нуль/нуле*/ноле*/0)} *
            q: * $pay * (назнач*/нужн*/необходим*/оговорён*/*фиксирован*/~один и (тоже/тот же)) * (время/день) *
            q: * {$rechargeBalance * [$Number] [числ*]} * {[~деньги] * $chargeBalance * $Number [числ*]} *
            q: * {(есть привычка/привык*) * $chargeBalance} *
            q: * {($noReason/не ~хотеть) * $rechargeBalance * $inTime} * [$why] *
            q: * {$rechargeBalance * $Number числ*} *
            q: * {$rechargeBalance * $inTime} * {($noReason/не ~хотеть) * $enable} *
            q: * {($noReason/не ~хотеть) * $enable} * {$rechargeBalance * $inTime} *
            q: * {(она/это*/$service) *  $noReason} * {[~любить] $pay * $inTime} * [{(приход*/присыл*/посыл*/отправ) * (смс*/sms/сообщение)}] *
            q: * {[~любить] $pay * $inTime} * {(она/это*/$service) * $noReason} *
            q: * {$pay * (честно/$inTime) * $noReason} *
            q: * {([в] $minus) * (загон*/заход*/уход*/попадать)} * не (хочу/могу/люблю/любим/~нравится) *
            q: * не (хочу/могу/~любить/~нравится) * {([в] $minus) * (загон*/заход*/уход*/попадать)} *
            q: * {{(достаточно/хватает/в избытке/столько/много/количество/~такой) ($money/~сумма)}  * (собрал*/собран*/лежит/на (счету/телефоне))} *
            q: * (успеваю/успеваем) *платить * [до того как/перед тем как] *
            q: * (сколько надо) [я] (*плач*) *
            q: * {$money * (на (счет*/баланс*)) сейчас} *
            script:
                $temp.definedEndStatus = "Всегда плачу вовремя";
                manageRepeatsAndAnswer([
                    "Я всегда вовремя плачу_UPD_01.06.wav",
                    // "PKU_30.03_Я всегда вовремя плачу (повтор)_01.wav"
                    "Я всегда вовремя плачу (повтор)_UPD_01.06.wav"
                ], true);

        # Просит повторить/назвать цену
        state: RepeatPrice
            q: * (день/сутки) *
            q: * оплата как *
            q: * {сколько (так*/стоит) * $unlimited} *
            q: * мой счет или * ваш *
            q: * не за что платить не буду
            q: * (не знаю/*дум*) * ($pay/сколько/стоить/~цена/$Number р) $weight<1.2> *
            q: * ($pay/сколько/стоить/~цена/$Number р) * (не знаю/*дум*) $weight<1.2> *
            q: * {(нужно/надо/должн*/должен/*требу*) * платить} * $weight<+0.1>
            q: * (что/сколько) * {(нужно/надо/должн*/должен/*требу*) * платить} * $weight<+0.1>
            q: * сколько рублей *
            q: * (деньги/ничего/средства) * (~снимать/сниматься) * [не (будут/будет*)] *
            q: * (деньги/ничего/средства/плат*/оплат*) * [не/будет*] * (~снимать/сниматься/~брать/~браться) * [если * ($activate/предостав*)] *
            q: * не так быстро *
            q: * $Number р [* (день/сутк*)] * [получается*] * [да]
            q: * $Number р [* (день/сутк*)] * [получается*] * да
            q: * (снимать*/списывать*) *
            q: * {[то есть/не понял*/подожди*/$pay] * ((сколько/~какой) * [стоит*/день/сутки]/$pay/дополнит* * ($pay/сумм*)/~цена)} *
            q: * {[то есть/не понял*/подожди*] * (не [надо/нужн*] плат*)} *
            q: * всего в день *
            q: * {[то есть/не понял*/подожди*] * (сколько/чего) * (стоит/сколько/[будет] [$AnyWord] (меся*/помесячн*))} *
            q: * {[то есть/не понял*/подожди*] * (сколько/чего/что) * (стоит/сколько/стоить)} *
            q: * {[то есть/не понял*/подожди*] * (сколько/чего) (ты/вы) (сказал*/говори*/горишь)} *
            q: * {[то есть/не понял*/подожди*] * [повтори*] * (сколько * (нужно/надо)) * $pay} *
            q: * [то есть/не понял*/подожди*] * {точно * (ничего * ~стоить/$free/безлимит*/не * ~платить)} *
            q: * [то есть/не понял*/подожди*] * ($free/безлимит*) *
            q: * [то есть/не понял*/подожди*] [повтори] * (стоить/цена/цену/стоимость) *
            q: * [то есть/не понял*/подожди*] * будет* * (стоить/~цена/стоимость) *
            q: * в день ((будут/станут) (снимать/списывать)/снимут/спишут/платить) *
            q: * {[[$disagree] нужн* $pay] * (это $2AnyWords платн* $service)} * $weight<+0.1>
            q: * что по чем *
            q: * {(денежк*/$money) * (надо/нужно/требуется/придется) ($pay/~скидывать)} *
            q: * (что/сколько) * {(нужно/надо/должн*/должен/*требу*) ($you/$you мегафон*)} * $weight<+0.1>
            q: * [$unlimitedAgree] * (процент*) *
            script:
                $temp.definedEndStatus = "Сколько стоит";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "08.04.21_Сколько стоит.wav",
                    "08.04.21_Сколько стоит (повтор).wav"
                ]);

        # Можно ли отключить
        state: CanITurnItOff?
            q: * {$turnOff * [можно/сможем/захоч*/потом/*могу/ногу/понадобится]} *
            q: * {$turnOff * (мегафон*/$office) * (если что/не понрав*)} *
            q: * {($turnOff/переделать) * (можно/*могу/ногу/сможем/захоч*/потом/*можем)} *
            q: * {(можно/*могу) * (сейчас * подключ*) * (потом * (отключ*/выруб*))} *
            q: * (это * обязательн*) *
            q: * не понравится *
            q: * не (понравится/устроит/устраивает) * (*могу/ногу/можно/сможем/потом/как) * [$turnOff]  *
            q: * {(*могу/ногу/можно/сможем/потом) * $turnOff} *
            q: * {потом * [(можно/*могу) $weight<+0.2>] * ($turnOff/вернуть*/возвратить*/откатить*) * [$lk]} *
            q: * если [мне/я] не понравится *
            q: * {потом (изменить/поменять) (можно/нельзя)} *
            q: * захо* $turnOff *
            q: * [$perfect] * {$lk * (отключ*/отличить/снять/выключ*/исключ*/убрать/удалить/отказаться/отмени*/управл*)} *
            q: * {(отключ*/отличить/снять/выключ*/исключ*/убрать/удалить/отказаться/отменить/переменить*/отменю/обратиться) * (как/где/чтобы/куда/{($needLocal/должен/должна) * *звонить}) * [потом/*могу] } *
            q: * {((если/чтобы) * (отключ*/отличить/снять/выключ*/исключ*/убрать/удалить/отказаться/отменить)) * ($needLocal/должен/должна) * *звонить вам} *
            q: * {не понравится * (как/где) (*могу/ногу/можно/сможем/потом) * [*ключить*/отказаться/убрать/удалить/избавиться]} *
            q: * если * что [$AnyWord] [$AnyWord] пойдет не так *
            q: * сейчас * подключ* * потом * $turnOff *
            q: * {(*могу*/*можем*) * $turnOff * (сам*/$later)} *
            script:
                $temp.definedEndStatus = "Смогу ли отключить";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "Смогу ли отключить_UPD_01.06.wav",
                    "Смогу ли отключить (Повтор)_UPD_01.06.wav"
                ]);

        # Как отключить
        # state: HowToTurnItOff?
        #     q: * {(отключ*/отличить/снять/выключ*/исключ*/убрать/удалить/отказаться/отменить/переменить*/отменю) * (как/где/чтобы) * [потом/смогу]} *
        #     q: * {(отключ*/отличить/снять/выключ*/исключ*/убрать/удалить/отказаться/отмени*) * (как/где/чтобы) * [потом/смогу]} *
        #     q: * {не понравится * (как/где) (*могу/ногу/можно/сможем/потом) * [*ключить*/отказаться/убрать/удалить/избавиться]} *
        #     q: * (как/каким) * || fromState = /Objections/CanITurnItOff?
        #     script:
        #         $temp.definedEndStatus = "Как отключить";

        # Какие условия
        state: Conditions
            q: * ([вам $weight<-1>] * повтор*|(не|плохо) слыш*|не расслыш*|еще раз*|что что|$what [вы/ты] сказа*|$dontUnderstand|непонятн*|{это как} понять|как как) * $weight<+0.1> || fromState = /Objections/Agreed
            q: * {европ* * (есть/доступ*)} *
            q: * {не понял* * повтор*} *
            q: * [так/$wait] * (повтор*|(не|плохо) слыш*|не расслыш*|еще раз*|что что|$what [вы/ты] сказа*|$dontUnderstand|непонятн*|{это как} понять|как как) *
            q: * (прослушал/в смысл*) *
            q: * {[только] * городск* * номер*} *
            q: * куда добав* *
            q: * [$dontUnderstand] * (что [это] за/какая/в чем [~суть/~смысл/состоит/заключается]) * ($service/на связи/~подарок) * [$repeat] * $weight<1.15>
            q!: * {[повтори*] * (услови*/преимуществ*) * [~какой/что [за/по]/повтори*/дополнит*]} * [использ* $2AnyWords $service] *
            q: * {(безлимит*/~безлимитный) * [мегафон*]} *
            q: * ([а] то есть) *
            q!: * {(сколько/количество) * *минут*} *
            q!: * ((что за/какая [еще]) * $service) *
            q: * ([а] то есть) *
            q: * (сколько/количество) * минут* *
            q: * [то есть/$dontUnderstand/$wait/еще раз*] * (~звонок/смс/как (это/он*) (работает/будет работать)/что (за/[вы/ты] {[$AnyWord] (хотите/хочешь/собирае*/планируе*)}) (предлож*/предлаг*/подключить/добавить/включить)) * $weight<+0.1>
            q: * [$dontUnderstand] (что [это] за/~какой [еще]/расскажи/расскажите) * ($service/комплимент*) *
            q: * {[подожди/погод*/секунд*] * (не (понял*/понимаю))} *
            q: * {~суть (не [очень] понял*)} *
            q: * {(не [очень] понял*) * (услови*/преимуществ*)} *
            q: [$botName] [а] {что [это] [еще] (за/такое)} [$botName] *
            q: * [$disagree] [объясн*] * {как (работает/будет работать) * (она/он/это/$service)} *
            q: * ((в чем) [конкретно/же] [$service] (состоит/заключается)) *
            q: * где * [конкретн*] ~предложение *
            q: * {(разъясни*/объясни*) $2AnyWords *подробн*} *
            q: * да (подождит*/обожди*/погоди*/стой*) *
            q: * {(подожди*/обожди*/минуточк*/(что/чуть) такое) * [что] [то] * не (понял*/понятн*)} *
            q: * (могут/может) * не устроить *
            q: * {(постоянн*/дополнительн*) (услуга/опция/подключен*)} *
            q: * {связь * не будет* блокировать*} *
            q: * как деньги * буд* списывать* *
            q: * что [вы] [сказали/говорите] *
            q: не (слушал*/расслышал*)
            # "повтори" распозналось как "3"
            q: (3/что)
            q: * {(повтори*/повторить) * [пожалуйста/можете/можно/могли]} *
            #q: (але/алло)
            q: * {[если/при] (нол*/нул*) * (*могу (звонить/пользоваться/не платить)/~связь * $later * ($money/$pay))} * $weight<+0.2>
            q: * {вопрос* возник*} * $pay *
            q: * {(сколько/месяц*/долго) * (*могу/можно) * не платить} *
            q: * {что * (она/он/нее/него/$service) * (включает/зависит) * [в себя]} *
            q: * {$internet * не * отключ*} *
            q: * {(когда/в какой момент/какого числа/в каком смысле) * (платить/оплачивать/пополн*/{(деньги/$Number [руб*]) * (класть/кладу/ложить/ложу/переводить/перевести)})} *
            q: * {[не] [должен/должна] * $pay * (в конце месяца/как [я] (плачу/оплачиваю))} *
            q: * [не (надо/нуж*)] * {кредит* [что ли] * не понял*} *
            q: * {что [вы/именно] * (предоставляете/предоставит*/предоставят*/предлагаете/представите)} *
            q: * {(плачу) * (разговариваю/говорю/ложу/кладу) * [$Number [руб*]]} $weight<+0.2> *
            q: * что* * подключ* [а] потом $weight<+0.2> *
            q: * {(можно/*могу) * ($pay/положить) * (*позже/потом/не сразу/[в] течени* месяц*/$Number [$Number] [числ*] $weight<+0.2>)} *
            q: * {(не (*надо*/нужн*/нужен)) [$AnyWord] $internet [на $phone]} $weight<+0.3>  *
            q: * (роуминг*/за границ*/друг* * город*) $weight<+0.5> *
            q: [$botName] [ну/а] [мне] {это [ли] обязательно} [$botName/подключать]
            q: [$botName] [ну/а] [мне] {это [ли] обязательно} [$botName/подключать] || fromState = "/Objections/Agreed"
            q: * давай* (*короче/коротко) *
            q: * {(это/[хотите] * (~сделать/~добавить)) * кредитн* лимит*} *
            q: * {(как/каким образом) * ($service/это/он/она/ее/его) * (устроена/работает/предостав*/будет работать)} *
            q: * {(как/каким образом/~какой) ($service/это/он/она/ее/его) $AnyWord (устроена/устроено/работает/будет (работать/выполнять))} * $weight<+0.2>
            q: * [очень/слишком] много информации *
            q: * {[можно/можете] [$2AnyWords] (*проще/простыми словами/*подробнее/*короче/*детальне*)} *
            q: * (как (понять/понимать)/то есть/получается/в [каком] смысле/значит) * {(в любо* (время/момент)/когда угодно) * (могу/можно) * ($pay/пополн*/{(класть/*ложить) [$money]})} * $weight<+0.1>
            q: * {$money * (на счет) * (*ложить/класть)} *
            q: * [то есть] {меня (к чему то) подключ*} *
            q: * {(какая/какую) ~сумма (предостав*)} *
            q: * ($disagree/надо) * лучше * (потом/позже/позднее) (заплачу/буду $pay) * $weight<+0.2>
            q: * {[это/$offeredProduct] (относится/касается) * (интернет*/мобильн*/~связь/звон*/телефон*/телеви*)} *
            q: {(все равно/[в] ~любой ~случай) * (буду/будем/надо/придется) * ($pay/~класть/~ложить/полож*/пополн*/закин*/закид*)} *
            q: * {(хочу/хочется/должна/должен) $2AnyWords (~понимать/~понять) * что $2AnyWords (соглаш*/соглас*/$activate)} *
            q: * {$free * повтори*} *
            q: *  если {[$2AnyWords] (раньше/сейчас/до этого) * $connection} * {$disconnect * [то] теперь} *
            q: * {[мегаф*/ты/вы/компани*] * $money * $rechargeBalance} * {(потом/затем/после/через/*позже/когда нибудь) * [я/мы/мне]} * {[должн*/нужн*] * $rechargeBalance} *
            q: * [это*] * {$tariff [на] котор* * сейчас} * {(он/$connection) * не * $disconnect [в конце месяц*]} * (продлев*/продлит*/продлят*) *
            q: * [этот] [$tariff] * [продлев*/продлив*] * не (понима*/пойму/поймём) [в] [чем/какой] * (смысл/суть) * [буду/будем] * [$emptyBalance] * [уход*] *
            q: * {(это*/эта) $service [$AnyWord] (получаетс*/выходит*/значит/следовательно/то есть)}
            q: [ты/вы] * {$enable * (нов*/дополнительн*/ещё/другой) * $service} *
            q: * {(какой/сколько) * (~отсрочка/~откладывать/~отсрочить) * [~платёж/~плата/~оплата]} *
            q: * {(не $pay) * (и так/уже/без того)} * {$Number рубл* * $pay} *
            q: * {(и так/уже/без того) * $Number рубл*} * {$pay * (не $pay)} *
            q: * [это как [это]] * {$when [~быть/произой*/происход*/$enable] * ($change/изменен*)} *
            q: * {(как (понять/понимать)/то есть/получается/в [каком] смысле/что значит) * {(могу/можно/возможно*) * $stayConnected}} *
            script:
                $temp.definedEndStatus = "Какие условия";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "Какие условия_UPD_01.06.wav",
                    "16.06_PKU_Предложение повтор.wav"
                ]);

        # Буду забывать платить
        state: WillForgetToPay
            #вороне = провороню
            q: * {(забывать/забуд*/проворон*/вороне*) * (платить/оплач*/оплат*/пополнять/запла*)} * $weight<+0.2>
            q: * {не [внесу] * (платить/оплач*/оплат*) * вовремя} *
            q: * [мне] * неудобно *
            q: * {(*пута*/таица/проще/удобно) * (какой месяц/когда) * $pay} *
            q: * [$disagree] * {(знаю/знать/знал*/не *помню) [когда] * (*платить/пополнять/класть/*ложить/баланс*)} * $weight<+0.1>
            q: * (забываю/забуду/~забыть) * баланс* *
            q: * {когда * $money (списались/списывают*) * (проще/легче)} *
            q: * {(забывать/забуд*) * (полож*/пополн*/перевод*/перевест*)} * {[$money] * ($money/счёт*/баланс*)} *
            q: * {[$money] * ($money/счёт*/баланс*)} * {(забывать/забуд*) * (полож*/пополн*/перевод*/перевест*)} *
            q: * {[$disagree] * [лучше/давай*] * [$turnOff]} * {(накоп*/скоп*/~копиться/образу*/образо*/возник*/~появляться/пойдёт/пойдут/выраст*/вгон*/гонит*/гонишь/набер*) * $emptyBalance} *
            q: * {(накоп*/скоп*/~копиться/образу*/образо*/возник*/~появляться/пойдёт/пойдут/выраст*/вгон*/будут/будет) * $emptyBalance} * {[$disagree] * [лучше/давай* * $turnOff]} *
            q: * [$stayConnected] * [$money] * {$emptyBalance * (накоп*/скоп*/~копиться/образу*/образо*/возник*/~появляться/пойдёт/пойдут/выраст*/вгон*/будут/будет)} *
            script:
                $temp.definedEndStatus = "Буду забывать платить";
                manageRepeatsAndAnswer([
                    "PKU_31.03_Буду забывать платить.wav"
                ], true);

        state: WhatIsYourName
            q: * {(тебя/вас/ваш*/~такой) $botName (зовут/звать/имя/фамили*)} *
            q: * {(тебя/вас/ваш*/~такой) * [мегафон*] (зовут/звать/имя/фамили*)} *
            q: * [скажи*] * (кто * (это/звонит/говор*)/представ*/кем * (говор*/разгов*/диалог)) *
            q: * {как* * (тебя/твое/вас/ваше) * (имя/зовут/звать/называ*)}
            q: * {кто (вы/ты/это) * (такие/такая/такой)} *
            q: * {[скажи*] (еще раз*/повтори*) * (имя/зовут/звать/называ*)}
            q: * (какой/какая/что за) * (компани*/компон*/фирм*) *
            q: * откуда * звони* *
            q: * кто вы такая *
            q: * (откудова/откуда) * (звоните/звонишь)
            q: * {как * (звать/зовут) [тебя/вас]} *
            q: * {как * (могу/вам) * обращаться*} *
            q: * {кто (вы/ты/это)} *
            q: * (ваше/твое) имя *
            q: * это [компания] мегафон* *
            q: это компания мегафон *
            q: * вы [из/работ* *] [компан*] мегафон* *
            q: * ты (из/работ* *) мегафон* *
            q: * {$botName * кто} *
            q: * [$girl] $botName ($questionMarkerAtTheEnd/[так] ведь)
            script:
                $temp.definedEndStatus = "Кто звонит после предложения";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "08.04.21_Кто звонит (после предложения).wav",
                    "16.06_PKU_Как_вас_зовут_кто_звонит_после_предложения_01.wav"
                ]);

        # Добавить опцию потом
        state: AddLater
            q: * {(если * нужно/если что/изучу) * сам*} *
            q: * {(почитать/посмотреть) * (должен/должна/надо/нужн*)} *
            q: * [$disagree [не (добавляй*/подключай*/активируй*)]] * [если (*над*/нужн*/*треб*)] * {[смогу] * (сам*/сами/потом/позже/не сейчас) * (сдела*/подключить/подключу/подключим/буду (подключать*/переключать*)/переключу/переключим/воспользу*/включ*/перезвоню/позвоню/перезвоним/позвоним) * [если что]} *
            q: * (*позж*/потом) * || fromState = ../Agreed/IsAgreed
            q: {давай* [$AnyWord] [$AnyWord] (*позж*/потом)} || fromState = ../Agreed/IsAgreed
            q: * {сначала (я/мы) (схожу/сходим)} *
            q: * обязательно сегодня *
            q: * (*позж*/потом) [потом] *
            q: * {сейчас * (пока/~болеть)  [потом]} *
            q: * {(перезван*/перезвон*/набер*/нужн*/надо) * (подумать/подумать/подумаю/подумаем/обдумать/обдумаем/поразмышл*)} *
            q: * {($lk/прилож*) * (*позж*/потом/посмотрю/посмотрим/почит*/прочит*)} $weight<+0.3> *
            q: * {(*позж*/потом/подумаю/я вам) * (позвон*/набер*/перезвон*)} *
            q: * {(я/мы) [$AnyWord] (вам/тебе) * (позвон*/набер*/перезвон*)} *
            q: * {если (нужно/*надо*) * (позвон*/набер*/перезвон*)} $weight<+0.3> *
            q: * (давай* * (*позж*/потом))
            q: * {(на неделе) * посмотр*} *
            q: * ([давай*/можно] * (*позж*/потом) * (добавим*/добавл*/добавить/подключ*/активирую*/активируе*)) *
            q: * {(посмотрю/подключ*/добавлю) * ($online/кабинет*/$lk/$internet/сайт*/$webPage) * [сам* (*ключу/активирую/добавлю)]} *
            q: * (потом/*позже/(другой/следующий) раз) * [можно] *
            q: * {(подключ*/активиров*/добав*) * $office} *
            q: * {(сам/сама/сами) [подключ*/добав*/оформ*] * [~личный ~кабинет]} *
            q: * [то есть/не понял*/подожди*] * {(подойд*/зайд*/приду/придем/подойти/зайти/прийти/заглян*/заглянуть/схожу/сходим/сходить) * (к вам/$office) * [если * нужно/если что/лучше] $weight<+0.1>} *
            q: * (посоветуюсь/посоветоваться/спрошу у $AnyWord) *
            q: * {(*совет*/~спросить/~узнать/*консультир*/разреш*) * (~парень/девушк*/~муж/~жена/~брат/~сестра/~сын/~дочь/~внук/~внучка)} $weight<+0.2> *
            q: * {(в $office) * (подключить/подключу/подключим/буду (подключать*/переключать*)/переключу/переключим/сделаю/$can [$AnyWord] сделать)} *
            q: * {(~сам/~сама/сами/потом/позже/сделаю) * (добав*/сдела*/подключить/подключу/подключим/буду (подключать*/переключать*)/переключу/переключим)} * $weight<+0.1>
            q: * {(~сам/~сама/~сами/потом/позже/сделаю) * (добавлю/подключу/ключу/подключим/буду (подключать*/переключать*)/переключу/переключим)} * $weight<+0.3>
            q: * (давай* * (*позж*/потом))
            q: * [$disagree] * {[если [будет] (надо/нужно/*требуется)] * подключ* * $lk} *
            q: * [$disagree] * {(перезвон*/перезвaн*/набер*) * (подключ*/если (что/*дум*/надо/нужно/понадобится))} * [$disagree] *
            q: * но не сейчас *
            q: * {пока * (не (буд*/жела*))} *
            q: * {(пока не) * (буд*/жела*)} *
            q: * [потом/*позже] * (заеду/заедем/зайд*) $weight<+0.2> *
            q: * [потом/*позже/если * (нужно/требуется/надо)/если что] * (заеду/заедем/зайд*/завтра) * $office $weight<+0.2> *
            q: * (подъеду/поеду/*йду/*йдем) * (обсуди*/судим/*узна*) * $weight<+0.3>
            q: * {сам * подключ* [если что]} * $weight<+0.3>
            q: * разбер* * (скажу/решу) * $weight<+0.1>
            q: * (доч*/сын*) * (скажу/спрошу/посовет*) *
            q: * {[я] (сначала/сперва/*подробн*) [всё] * (*узнаю/*смотр*/провер*/изучу/поспрашиваю)} $weight<+0.3> *
            q: * {[я] давай* [всё] * (*узнаю/*смотр*/провер*/изучу/поспрашиваю)} $weight<+0.1> *
            q: * [$repeat<$disagree> * [подключ*/добав*/активир*/включ*]] * {я [$AnyWord] сам* (потом/как нибудь)} * [$repeat<$disagree>] * $weight<+0.1>
            q: * {(хочу/проще) (изучить/посмотреть/глянуть) * (сам*/$tariff)} *
            q: * [$no] [$thanks] * {(когда/если) * (понад*/надо/нужно/потреб*/необходим*/~хотеть/зохот*/захоч*/*интерес*) * $reachYou}  *
            script:
                $temp.definedEndStatus = "Потом сам подключу";
                manageRepeatsAndAnswer([
                    "PKU_30.03_Сам подключу_01.wav",
                    // "PKU_30.03_Сам подключу (повтор)_01.wav"
                    "16.06_PKU_Сам подключу (повтор)_01.wav"
                ], true);

        # На какой номер звоните?
        state: WhatNumber
            q: * {(~какой/~который) * (номер*/сим*/сем*) * [звоните/звонишь/говорите/говоришь/мегафон*] * [сейчас]} *
            q: * {(несколько/много/две/три/не одна/не один/два) * (номер*/сим*/сем*) * [сейчас] [у меня]} *
            script:
                $temp.definedEndStatus = "На какой номер звоните";
                $session.questionAsked = true;
                var phoneNumber = $dialer.getCaller();
                var firstDigit = phoneNumber.substring(9, 10);
                var lastDigit = phoneNumber.substring(10);
                $temp.numberPartOne = getAudioDigit(firstDigit, true);
                $temp.numberPartTwo = getAudioDigit(lastDigit);
            # audio: PKU_30.03_На какой номер звоните_01.wav
            audio: 31.03_На какой номер звоните.wav
            audio: {{ $temp.numberPartOne }}
            audio: {{ $temp.numberPartTwo }}
            audio: 31.03_На какой номер звоните - концовка.wav

        state: AfraidToBeOverdrawn
            q: * {(боюсь|не (надо/нужно/хочу)) * (уйти|уходить|уходил*|чтобы/нужно/хочу/надо/довод*/доход*/оказат*/оказыват*) * [в] (минус*|$regexp<->|минут|(меньше|ниже|до) (нол*|нул*|0))} * $weight<+0.3>
            q: * {(боюсь|не (хочу/живу/нужн*/привык*/беру/находит*/*станов*/собира*/набира*/скаплива*/люблю/нравится)|будет|станет|страшно|вогнат*|посадите|останусь|останемся|копиться|накапливаться) * (долг*/*долж*/*далж*)} * $weight<+0.3>
            q: * [$disagree] не (хочу/хотим/люблю/любим/собираюсь/собираемся/желаю/желаем) * {жить [в] долг*} [$disagree] *
            q: * {(боюсь|не хочу|будет|станет/смуща*) * (отрицательн* баланс*|минус*)} * $weight<+0.2>
            q: * {(будет|буду|смущает|не хоч*|страшно|уходил*|не знаешь [что]|не устраивает) * (минус*|$regexp<->) * [долг*]} * $weight<+0.2>
            q: * {деньги * оказ* * (чужие/не твои)} *
            q: * {(*звонит*/*разговар*/*общаться/*говори*) * (при (нол*|нул*|0))} $weight<+0.4> *
            q: * {(появ*/образуются/образовыва*/не (буду/набрал*/набирал*)) * (долг*/задолж*)} $weight<+0.3> *
            q: * {[буд*] * $emptyBalance} * {(залаз*/залез*/~попадать/~попасть/оказ*/окаж*) * [$later]} *
            q: * {[хочу/люблю/предпочитаю/живу] [жить] (по средствам)} [{(не ~жить) (в долг) [я/мы]}] * [не $need] *
            script:
                $temp.definedEndStatus = "Боюсь уйти в минус";
                manageRepeatsAndAnswer([
                    "Я не хочу в минус_UPD_01.06.wav"
                ], true);

        state: Anger
            q: $stateComplaint
            audio: PKU_30.03_Абонент отказался_01.wav
            script:
                $temp.definedEndStatus = "Абонент ругается";
                hangUp();

        # На сколько дней
        state: WhatIsTheDuration
            q: * (это/он/она/так) * {(ежемес*/[кажд*] месяц*/постоянн*/временн*) * [будет]} *
            q: * {(это/он/она/так) * (ежемес*/[кажд*/след*] месяц*/постоянн*/временн*)} * [будет] *
            q: * [до] {(какой/кого/какого/какие/определен*) * (период [врем*]/времен*/момент*)} *
            q: * {((какой/какие) (срок*/период)/надолго/долго) * (подключ*/добавля*/длиться/длительность/продолжительн*)} *
            q: * сколько * тред *
            q: * {сколько дней * [скидк*]} *
            q: * [на] (сколько (дней/месяцев)|какое время) *
            q: * [это [будет]/будет] (насовсем/на совсем) *
            q: * какой [то] срок* *
            q: * (как долго/[на] какой срок*) * [будет/продлится/длиться/действовать] *
            q: * (сколько/сколько [по] врем*) * (продлится/продлить*/~длиться/действовать/тред/активир*/(будет) (идти/продолж*/[$AnyWord] безлимит*)) *
            q: * (навсегда/на всегда) *
            q: * {(срок*/период/надолго/долго/длительн*/длит*/надолго/постоян*/сколько [по] врем*) $2AnyWords ~акция}  *
            q: * {(когда/срок/как скоро/скоро) (заверш*/законч*/конец/конч*) $2AnyWords ~акция}  *
            q: * [как] * {только * (один/этот/этом/на) месяц*} *
            q: * до (конца/окончания) $2AnyWords (месяц*/год*) *
            q: * {[$service/это/~он] * [до] следующ* ~месяц} * ($enable/работ*) *
            q: * ($enable/работ*) * {($service/это/~он) * [до] следующ* ~месяц} *
            script:
                $temp.definedEndStatus = "Срок акции";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "Длительность акции БНС 05.11.wav"
                ]);

        # Мне так неудобно
        state: Inconvenient
            q:  * {[мне] (неудобн*/не $2AnyWords (удобн*/комфортн*)/некомфортн*) * [$disagree] * [$unlimitedThanks/$minus]} * $weight<+0.1>
            q:  * {[мне] (неудобн*/не $2AnyWords (удобн*/комфортн*)/некомфортн*) * [$service/$minus]} *
            q: * {[мне] (удобно/удобнее) * (как (есть/сейчас)/без * $service/[знать $2AnyWords что $2AnyWords/(один/раз [в]) (~день/~месяц)/одно врем*] $pay)} *
            q: * {$disagree * (неудобн*/не $2AnyWords (удобн*/комфортн*)/некомфортн*) * (уходить в $minus)} * $weight<+0.2>
            q: * {(у (меня/нас) [$2AnyWords] (нет/нету/отсутств*)) * (не ($needLocal/нужд*))} *
            q: * {(нет/нету/отсутств*) * (не ($needLocal/нужд*) [в] [~это])} *
            script:
                $temp.definedEndStatus = "Мне так неудобно";
                manageRepeatsAndAnswer([
                    "Мне так неудобно_UPD_01.06.wav"
                ], true);

        # Когда лимит будет исчерпан
        state: LimitEnd
            q: * {(лимит*/долг*/баланс*) * (исчерп*/законч*/превыс*/превыш*)} *
            q: * {что будет * (после/когда) * *конч* лимит*} *
            q: * [абсолютно бесплатно] * [пополн*] * {как* * (узнаю/*знать/пойму/понять)} * {(надо/нужно/пора/когда) * (пополн*/$pay/*кончил*/*конча*)} *
            q: * {{(как*/не) * (узнаю/*знать/пойму/~знать/~узнать/[должен/должна] (понима*/понять))} * {(надо/нужно/пора/когда) * (пополн*/$pay/*кончил*/*конча*/{[$money] * (положить/*бросить/отправлять)})}} * $weight<1.1>
            q: * (узнаю/*знать/пойму/~знать/~узнать/понима*/понять) что * {(надо/нужно/пора/когда) * (пополн*/$pay/*кончил*/*конча*/{[$money] * (положить/*бросить/отправлять)})} *
            q: * когда * деньги * закончатся *
            q: * {предупрежд* * [что] * (списа*/$pay/пополн*/пусть/пускай)} * $weight<+0.2>
            q: * потом * (на/в) $minus не (пойдет/уйдет) *
            q: * {как * (пополн* счёт*/(полож*/ложи*) ~деньги)} * не * (~знать/~понимать) * ~деньги * (нет/закончи*/кончи*) *
            q: * {как * (пополн* счёт*/(полож*/ложи*) ~деньги)} * ~деньги * (нет/закончи*/кончи*) *  не * (~знать/~понимать) *
            q: * {(смс*/оповещ*/сообщ*) * (что/когда)} * (там/лимит*/долг*/баланс*) *
            q: * (смс*/оповещ*/сообщ*) * {(что/когда) * [у] [меня] * (там/лимит*/долг*/баланс*/происходит/творится/остат*/осталось)} *
            script:
                $temp.definedEndStatus = "Когда лимит будет исчерпан";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "3_06_PKU_когда_лимит_будет_исчерпан.wav"
                ]);

        # Что будет, если не пополню баланс вовремя
        state: WhatIfIDontPayInTime
            q: * {[что] * если * (не [$AnyWord] (пополн*/*плачу/полож* ден*/$pay)/упущу момент) * [вовремя/во время]} * $weight<+0.1>
            q: * {если * не * до двадцать шестого} *
            q: * если * пропущу * момент * (пополн*/оплат*) *
            q: * если * {(пропущ*/пропуст*/забуд*/упущу момент) * (полож*/пополн*)} * {(через/на) *  (месяц*/~день/сутк*/недел*)} *
            q: * если * {(через/на) *  (месяц*/~день/сутк*/недел*)} * {(пропущ*/пропуст*/забуд*/упущу момент) * (полож*/пополн*)}  *
            script:
                $temp.definedEndStatus = "Что если не пополнить вовремя";
                $session.questionAsked = true;
                manageRepeatsAndAnswer([
                    "3_06_PKU_что_будет_если_не_пополню.wav"
                ]);

        state: AnotherInfo
            q: * {[другой/никто/почему] * [(до меня/нас)/мне/нас] * не * ~дозвониться} *
            q: * {*звон* * друг* регион*} *
            q: * {($cannot/не (смог*/получил*/~прийти/приход*/установ*/устанав*)) [~пароль/детализаци*] $2AnyWords (лк/[личн*] кабинет*/аккаунт*)} *
            q: * {(~пароль/детализаци*/~баланс) * (лк/[личн*] кабинет*/аккаунт*)} *
            q: * {~какой еще $2AnyWords (есть/там) * $service} *
            q: * {есть * ~другой $service} *
            q: * (где/у кого) * узнать *
            q: * {(вопрос*/*консул*/узнать/спросить/расскажи*/информ*) * (~подписка/(не (по/про/о/об)/(по/про/о) ~другой) $2AnyWords ($service/$promo/$discount))} *
            q: * {(вопрос*/*консул*/узнать/спросить/расскажи*/информ*) * (по/про/о) $2AnyWords ($service/~подписка) * (подключен*/подключили)} *
            q: * {(~какой/список) $2AnyWords $service * (подключен*/подключил*)} *
            q: * {$cannot (дозвон*) * (специалист*|оператор*|мегафон*|колл центр*)} *
            q: * {(переключи|переключите|переведи*) * (специалист*|оператор*|колл центр*)} *
            q: *  {(~вернуть|[сделать] возврат*) * [списанные|снятые] $money} *
            q: * {(удали*|отписаться) * (~подписка|приложени*)} *
            q: * {восстановить доступ * [лк/[личн*] кабинет*/аккаунт*]} *
            q: * {~подписка * (возник*/подключили/подключен*/~появиться)} * $weight<+0.2>
            q: * (вопрос*/*консул*/узнать/спросить/расскажи*/информ*/хотел*/хочу) * {~другой $tariff} *
            q: * {что  * сейчас * подключен*} *
            q: * {вы мне * *долж*} * [рубл*] *
            q: * {$tariff * (*могу/можно/може*) * (смен*/помен*/измени*) } *
            q: * {[[по] (повод*/вопрос*)] *  номер* * (остав*/остался)} *
            q: * {(был/сменили/поменяли/присвоили) $2AnyWords $tariff} * [за] $Number [руб*] * (теперь/стал/сейчас) *  [за] $Number [руб*] *
            q: * {(сменили/поменяли/присвоили) $2AnyWords $tariff * (не (спросив/спрашивая/предупредив)/без предупреждени*)} *
            q: * {(верните/отдайте/когда (вернете/отдадите)) * (долг*/задолженност*/$money)} * $weight<+0.4>
            q: * вот {[меня/нас] * есть * [еще] * номер*} *
            q: * [вопрос*] * {(предл*/подключ*/~есть) * [$unlimited] $internet} *
            q: * {(смс*/сообщен*) * (~прийти/приход*)} [{что * это}] * $weight<1.1>
            q: * [так] [а] вы (узнал*/знае*) (зачем/почему) *
            q: * $why * [у] (меня/нас) {*плат* [$AnyWord] [$AnyWord] $tariff} {(на [$Number] ~день) раньше} *
            q: * $why * [у] (меня/нас) {(на [$Number] ~день) раньше} {*плат* [$AnyWord] [$AnyWord] $tariff} *
            q: * $why * {(на [$Number] ~день) раньше} [у] (меня/нас) {*плат* [$AnyWord] [$AnyWord] $tariff} *
            q: * [как/что] у вас [за] такая [$AnyWord] система *
            q: * {$tariff * $change01 * (на другой)} *
            q: * если * {(захотим/захочу/захочется) $fromMegafon ($leave01/$giveUp01)} *
            q: * {$internet * (~другой ~связь)} *
            q: * {$internet * $change * минут*}
            q: * (как/каким/образом) * [$can] заблокировать * (телефон*/номер*) *
            q: * [лучше/$can] * {(верни*/вернуть/сделай*) * $discount * [как (раньше/прежде)]} *
            q: * {[еще] * (хотел*/хочу/хотим/$can/хотелось [бы]) * (спросить/узнать/ответить/рассказать) (про *)} *
            q: * {([~какой [то]] * ($tariff/программ*)) * ([у] вас) * ($Number $rub)} *
            q: * {$megafon * (подключил*/придумал*/включил*/активировал*) [~какой то] (мне/нам/[у] (меня/нас)) ($service/программ* $weight<+0.1>)} * [$Number $rub] *
            q: * {(писал*/подавал*) (заявк*/заявлени*/жалоб*)} * [[к] вам] *
            script:
                $session.questionAsked = true;
                $temp.definedEndStatus = "Где узнать другую информацию?";
                manageRepeatsAndAnswer([
                    // "10.06_PKU_another_information.wav"
                    "13.10_r_Оля - Где узнать другую информацию.wav"
                ]);

        # Перезвоните позже
        state: CallBackLater
            q: $callback
            q: * [давай*] * (времени (нет/нету)/{(за ребенком) (нужно/надо) (идти/бежать/ехать)}) *
            q: * [давай*] * {(позвони/позвоните/позвонить/давай*/*говор*) * (потом/позже/позднее/попозже/(след* раз)) * [занят*]} *
            q: * не (могу/можем) * (говорить/разговаривать) *
            q: * не (смогу/сможем) * (говорить/разговаривать) *
            q: * не [очень/совсем] удобно * (говорить/разговаривать) *
            q: * {(~перезвонить/позвони*) * (потом/*позже/позднее)} *
            q: * {~перезвонить * (можете/попросить)} *
            q: * {(я/мы) [в] (дороге/дороги)} *
            q: * (жду звонка/ожидаю звонок) *
            q: * давай* * (потом/*позже/позднее/через (полчас*/ * минут* */час*)) *
            q: * в (движении/другое время) *
            q: * (не (могу/можем)/нет (времени/возможности)) * (говорить/разговарить) *
            q: * {некогда * [совсем]} *
            q: * {нет врем*} * $weight<+0.5>
            q: * {(минут*/час*/~день) * через * $Number} *
            q: * [$disagree] * [перезвон*] * {[я/меня/мы/нас] * (на работе/работаем/работаю/на встрече/рабоч* ~день/занят*/за рулем)} * $weight<+0.2>
            q: * мне [сейчас] не до этого *
            q: * у меня [сейчас/полно] [$AnyWord] (дел*/работ*/рабоч*) * $weight<+0.2>
            q: * {[я] * занят* * с детьми} * $weight<+0.5>
            q: * {[не (могу/хочу)] на улице [{ничего (не слышу)}]} *
            q: * {[я] * (в магазин*/на кассе/[в] метро*)} $weight<+0.2> *
            q: * [да я/$unlimitedAgree] [я/мне] (занят*/не могу/не вовремя/невовремя/не [очень/совсем] удобно/неудобно/некогда) *
            q: * (болею/болеем/заболел*/болен/больна/[в] (больниц*/поликлиник*)/температур*) *
            q: * {говори* *быстр*} * $weight<+0.15>
            q: * {(не (могу/можем)) * [потреблять] (стою/стоим)} *
            q: * {(не (могу/можем)/я/мы) [я/мы] ([в] патрул*)} *
            q: * {($phone/аккумулятор*) * (разряжается/разрядится/садится/сядет/на (ноле/нуле))} *
            q: * {(могу/можем) [говорить/разговаривать] только [$AnyWord] [ран*/поздн*] (утром/днем/вечером/ночью/с утра)} *
            q: * {[я] * (занят*/под капельницей/баранку кручу/таксую/[управляю] (авто/машиной/автомобил*))} * $weight<+0.2>
            q: * {(не [$AnyWord]) подход* (время/момент)} *
            q: * [давай*] * {[маленько/*много/немножко] (занят*/не вовремя/невовремя/некогда/не [очень] удобно/неудобно)} * [{$later (*звони*/давай*)} *]
            q: $callbackDateTime $weight<-0.1>
            q: $callback $weight<-0.1>
            if: !$client.callAttempt
                # при последнем звонке $client.callAttempt удаляется
                audio: PKU_30.03_Абонент отказался_01.wav
                script:
                    $temp.definedEndStatus = "Три раза не было времени выслушать";
                    hangUp();
            else:
                audio: 31.03_Перезвоним позже.wav
                script:
                    $temp.definedEndStatus = "CALLBACK";
                    setRedialTime();
                    hangUp();

        # пришлось продублировать стейт, т.к. иначе не работали возражения после перехода в кечол
        state: Abroad
            q: * $abroad
            q: ((в/на) $Country/за границей/заграница)
            if: ($parseTree.value === 'notInCountry' && $parseTree.Country[0].value.name === 'Россия') || ($parseTree.value !== 'notInCountry' && ($parseTree.Country ? $parseTree.Country[0].value.name !== 'Россия' : true))

                audio: PKU_30.03_Абонент отказался_01.wav
                script:
                    log("Client is abroad");
                    $temp.definedEndStatus = "За границей";
                    hangUp();

            else:
                go!: /CatchAll

        state: ConnectionZeroBalance
            q: * [если] * [долж*] [срок] [заплат*] * {$connection * ($emptyBalance/не  [$AnyWord] [$AnyWord] [$AnyWord] $pay) * $disconnect} *
            q: * [если] * [долж*] [срок] [заплат*] * ([при] $emptyBalance/не  [$AnyWord] [$AnyWord] [$AnyWord] $pay) * {$connection * $disconnect} *
            q: * [если] * [~money] * {[буд*] * $stayConnected} * {[можно/продолж*] * $emptyBalance} * $weight<+0.1>
            q: * [если] * [~money] * {[буд*] * $emptyBalance} * {[можно/продолж*] * $stayConnected} * $weight<+0.1>
            q: * {[если] * $emptyBalance} * {[буд*] * [~money]} * {[$can/продолж*] * $stayConnected} * $weight<+0.1>
            q: * (если/есть ли) у меня ([будет] минус/{закончи* * (пакет/минут*)}) * $weight<+0.3>
            q: * {$connection * (~оставаться|~оставить) * $emptyBalance} *
            q: * {[чтоб*] * (*могу/*можем/*можно*/[даже/и] при/не (отключае*/выключае*)) * $emptyBalance} *
            q: * [при/в/если/когда] $emptyBalance * {(тоже/это/значит/получается/все) [$AnyWord] будет [работать]} * $weight<+0.1>
            q: * [при/в/если/когда] $emptyBalance * {[тоже/это/значит/получается/все] [$AnyWord] будет * работать} * $weight<+0.1>
            q: * [значит/(то есть)/получается] * {(можно/*могу) * (*звонить/{(принимать/принять) звонк*})} *
            q: * {(можно/*могу/*можем) * (*звонить/{(принимать/принять) звонк*}) * ((если/есть ли) * ([будет] минус/{закончи* * (пакет/минут*)}))} *
            q: * {(класть/ложить/$pay) * можно * ([в] ~конец ~месяц)} *
            q: * {(это/то есть/типа/получается/всё верно) * $emptyBalance * (~ходить/~уходить)} *
            q: * {$can * $pay * [конц* (текущ*/след*) месяц*/любой (~день/~дата)]} *
            script:
                $session.questionAsked = true;
                $temp.definedEndStatus = "Связь отключится с минусом?";
                manageRepeatsAndAnswer([
                    "PKU_22.06_Связь_отключится_с_минусом.wav"
                ]);

        state: IfOutOfMinutes
            q: * {([что] если/когда/в случае) * (минут*/$internet/$gigaMegabyte) * (законч*/конч*/~потратить/~истратить/использова*/исчерп*)} *
            script:
                $session.questionAsked = true;
                $temp.definedEndStatus = "А если у меня закончатся минуты / интернет?";
                manageRepeatsAndAnswer([
                    "08.04.21_А если у меня закончатся минуты , интернет.wav"
                ]);

        # Можно вопрос?
        state: Question
            q: * {[$can/$need/~хотеть/~хотеться/дайте/даете/пытаюсь] [$2AnyWords] (задать/задам/спросить/спрошу) [вам/тебе/[у] (вас/тебя)] вопрос*} *
            q: * [давай*/$can] * ($Number/пар*) слов* *
            q: [$AnyWord] {(можно/[у] (меня/нас)) [$2AnyWords] вопрос*} *
            q: [$AnyWord/*] {([*может*/можешь/готов*] [мне/нам] (ответить/ответ*/получить)) * (на [$2AnyWords] вопрос*)} [или нет] *
            q: * {($can/$need/~хотеть/~хотеться/дайте/давайте/пытаюсь/дадите/даете) [$2AnyWords] [[у] (вас/тебя)] (~уточнить/~спросить/~поинтересоваться/[$AnyWord] вопрос*/~узнать)} *
            q: (подожди*/подари*) подожди* *
            script:
               $temp.definedEndStatus = "Можно вопрос?";
               $session.questionAsked = true;
               manageRepeatsAndAnswer(["30.10_ЗАЩИТА_Можно вопрос.wav"]);

    state: CatchAll || noContext = true
        event: speechNotRecognized
        event: noMatch
        event: timeLimit
        q!: *
        q!: * {меня * (слыш*)} *
        q: * {всем * (предоставляете/активируете/добавляете)} * || fromState = "/Objections"
        q: * ничего себе *
        # q: * алло *
        q: $pickUpPhrase
        script:
            $session.timesOfCatchAll = ($session.timesOfCatchAll) ? $session.timesOfCatchAll + 1 : 1;
        if: $session.timesOfCatchAll > CATCHALL_LIMIT
            audio: PKU_30.03_В итоге ничего не расслышали_01.wav
            script:
                $temp.definedEndStatus = "CALLBACK";
                setRedialTime();
                hangUp();
        else:
            script:
                $temp.definedEndStatus = "Не расслышали";
                manageRepeatsAndAnswer(["16.06_PKU_Не расслышали - 1_01.wav",
                                        // "PKU_30.03_Не расслышали-1_01.wav",
                                        "PKU_30.03_Не расслышали-2_01.wav",
                                        "PKU_30.03_Не расслышали-3_01.wav",
                                        // "PKU_30.03_Не расслышали-4_01.wav",
                                        "16.06_PKU_Не расслышали- 4.wav",
                                        // "PKU_30.03_Не расслышали-5_01.wav"
                                        "16.06_PKU_Не расслышали - 5_01.wav"]);

    # Прогрев аудио
    state: CachingAudio
        q!: {(*греть/тест/закешировать/закэшировать) (аудио/звук*/музык*)}
        audio: 16.06_PKU_В чем моя выгода.wav
        audio: 16.06_PKU_Где можно почитать об опции.wav
        audio: 16.06_PKU_Знаю_я_вас,_начнете_деньги_сейчас_снимать_01.wav
        audio: 16.06_PKU_Как_вас_зовут_кто_звонит_после_предложения_01.wav
        audio: 16.06_PKU_Кто звонит (после предложения)_01.wav
        audio: 16.06_PKU_Не расслышали - 1_01.wav
        audio: 16.06_PKU_Не расслышали - 5_01.wav
        audio: 16.06_PKU_Не расслышали- 4.wav
        audio: 16.06_PKU_Общее возражение 1_01.wav
        audio: 16.06_PKU_Общее возражение 2_01.wav
        audio: 16.06_PKU_Опция всегда будет бесплатной_01.wav
        audio: 16.06_PKU_Опция_всегда_будет_бесплатной_повтор_01.wav
        audio: 16.06_PKU_Предложение повтор.wav
        audio: 16.06_PKU_Предложение_01.wav
        audio: 16.06_PKU_Сам подключу (повтор)_01.wav
        audio: 16.06_PKU_Сеть плохо ловит (повтор)_01.wav
        audio: 16.06_PKU_Сколько стоит (повтор)_01.wav
        audio: 16.06_PKU_Условия 1.wav
        audio: 16.06_PKU_Что изменится_01.wav
        audio: 16.06_PKU_Я всегда вовремя плачу (повтор)_01.wav
        audio: PKU_22.06_быть_на_связи_заключит_после_согласия.wav
        audio: PKU_22.06_Связь_отключится_с_минусом.wav


    state: CreateError
        q!: внутренняя проверка обработки ошибок
        script:
            throw "test error processing";

    state: CreateErrorRedial
        q!: внутренняя проверка обработки ошибок перезвон
        script:
            if ($request.channelType === "resterisk") $dialer.redial({startDateTime: "строка"});
