function hangUp() {
    var callResult = $.temp.definedEndStatus || $.temp.endStatusByAnswer;

    if (!callResult.startsWith("CALLBACK")) {
        delete $jsapi.context().client.callAttempt;
    }

    $.temp.botHangUp = true;

    if (!testMode()) {
        addPause(2);
    }

    $dialer.hangUp("(Бот повесил трубку)");
}

function getNowTimestamp() {
    var nowtime = $jsapi.dateForZone("Europe/Moscow", "yyyy:MM:dd HH:mm");
    var nowTimeStamp = moment(nowtime, "YYYY:MM:DD HH:mm");
    return nowTimeStamp;
}

function getSum() {
    if ($.request.channelType !== "resterisk") return "300";

    var sum = $dialer.getPayload().camp;
    sum = sum.toString();
    return sum;
}

function getAudioDigit(digit, first) {
    var audioName = "PKU_30.03_";
    audioName += first ? "Первый " : "Второй ";
    audioName += digit + ".wav";
    return audioName;
}

function getAnswerWithSum(sum) {
    var answer;
    switch (sum) {
        case "200":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_200.wav";
            break;
        case "300":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_300.wav";
            break;
        case "400":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_400.wav";
            break;
        case "500":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_500.wav";
            break;
        case "600":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_600.wav";
            break;
        case "700":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_700.wav";
            break;
        case "800":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_800.wav";
            break;
        case "900":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_900.wav";
            break;
        case "1000":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1000.wav";
            break;
        case "1100":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1100.wav";
            break;
        case "1200":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1200.wav";
            break;
        case "1300":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1300.wav";
            break;
        case "1400":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1400.wav";
            break;
        case "1500":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1500.wav";
            break;
        case "1600":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1600.wav";
            break;
        case "1700":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1700.wav";
            break;
        case "1800":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1800.wav";
            break;
        case "1900":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_1900.wav";
            break;
        case "2000":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_2000.wav";
            break;
        case "2100":
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_2100.wav";
            break;
        default:
            answer = "https://248305.selcdn.ru/demo_bot_static/PKU_30.03_200.wav";
            break;
    }
    return answer;
}

// Функция для выдачи ответов с учётом повторов и возражений
function manageRepeatsAndAnswer(arr, objection) {
    // Если это возражение, сначала вызываем обработчик. Если лимит достигнут, то останавливаем выполнение
    if (objection) handleNo();
    if ($.temp.transition && $.temp.transition.state === "/Objections/No/LastTime") return;

    $.session.repeatsInfo = $.session.repeatsInfo || {};

    if (Object.keys($.session.repeatsInfo).indexOf($.currentState) !== -1) {
        $.session.repeatsInfo[$.currentState] += 1;
        var inx = $.session.repeatsInfo[$.currentState];

        // Если уже заходили в стейт несколько раз и ответы кончились
        if (inx > arr.length - 1) {
            // Если это возражение, обратно уменьшаем timesOfLongNo и обрабатываем как общий отказ
            if (objection && !$.temp.repeat) {
                $.session.timesOfLongNo -= 1;
                $reactions.transition("/Objections/No");
            // Если это вопрос, заново берём ответы по порядку
            } else {
                $.session.repeatsInfo[$.currentState] = 0;
                $reactions.audio(arr[0]);
            }
        // Если уже заходили в стейт, но ответы ещё не кончились, отвечаем следующим доступным ответом
        } else {
            $reactions.audio(arr[inx]);
        }
    // Если заходим в стейт первый раз, заносим его в справочник с нулевым счётчиком и отвечаем первым ответом
    } else {
        $.session.repeatsInfo[$.currentState] = 0;
        $reactions.audio(arr[0]);
    }
}

function handleIfItWasNoInterest() {
    if ($.session.noInterest && !$.session.proposalWasMade) {
        $reactions.transition({value: "/Start/NoInterest", deferred: true});
    }
}
